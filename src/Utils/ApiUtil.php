<?php

namespace App\Utils;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
//use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\JgmLog;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ApiUtil
{
    public static $endpoint = "http://localhost/stirapi/public/index.php/api/";

    public static $apiKey = "dse.91295A27F6216C8DB98C969D4FAD8";
    public static $app_id = "infojgm";

    private $session;
    private $container;
    private $request;

    public function __construct(SessionInterface $session, Request $request, ContainerInterface $container)
    {
        $this->session = $session;
        $this->request = $request;
        $this->container = $container;
    }

    public static function getDiputados(){
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "diputados/activos");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            //print_r($j);
            //die("!");
            //if($j["success"]){
                
            //}
        }
        return $j;
    }

    public static function getInterbloques(){
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "interbloques/activos");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j;
        }
        
    }
    
    public static function getUltimoInforme(){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        try {

            $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "jgm/informes/activo");
            if($res->getStatusCode()==200) {
                $j = $res->getBody();
                $j = json_decode($j, true);
                if($j["success"]){
                    $d = $j["message"];
                }
            }

        }catch (\Exception $e){
            die($e->getMessage());
        }

        return $d;
    }

    public static function getInformesCerrados(){
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "jgm/informes/cerrados/historial");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                $d = $j["message"];
            }
        }
        return $d;
    }

    public static function getRequerimientosByNumeroInforme($numero){
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "jgm/informes/cerrados/historial/".$numero."/requerimientos");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                $d = $j["message"];
            }
        }
        return $d;
    }
    
    public static function getBloquesActivos(){
        $b = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "bloques/activos");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j;
        }
    }

    public static function getBloquesActivosSinInterbloque(){
        $b = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "bloques/activos/sin/interbloque");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j;

        }
    }
    
    public function getBloqueById($id){
        $b = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "bloques/".$id);
        
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j;
        }
        return $b;
    }

    public function getInterbloqueById($id){
        $b = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "interbloques/".$id);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j["message"];
        }
        return $b;
    }


    public function saveRequerimiento($requerimiento){
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/nuevo-requerimiento", [
            'json' => $requerimiento
        ]);

        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                $this->saveLog('requerimiento_guardado');
                return $this->returnMensaje('success', "Requerimiento guardado extisosamente.");
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);
    }

    public function deleteInforme($id){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('DELETE',$_ENV['ENDPOINT_STIRAPI'] . "jgm/informes/delete/".$id);
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){

                $this->saveLog('informe_borrado');
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error eliminando el informe");

    }

    public function saveInforme($informe){
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('POST', $_ENV['ENDPOINT_STIRAPI'] . "jgm/informes/nuevo-informe", [
                        'form_params' => [
                        'numero' => $informe["numero"],
                        'date-start' => $informe["fecha_inicio"],
                        'date-end' => $informe["fecha_fin"],
                        'date-visita' => $informe["fecha_visita"],
                        'expediente' => $informe["expediente"]
                    ]
                ]);
        
        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){

                $this->saveLog('informe_guardado');
                return $this->returnMensaje('success', $j["message"]);
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);
    }
    
    public function getRequerimientosByUserInformeActivo($user){

        $guid = $user["guid"];

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
    
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/".$guid.'/informe-actual');

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function getRequerimientosByEnvioRequerimiento($envioRequerimiento){

        //var_dump($envioRequerimiento);
        //die();

        $envioRequerimientoId = $envioRequerimiento["id"];

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/envio-requerimiento/".$envioRequerimientoId);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function getRequerimientosById($id){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/id/".$id);
        //return ApiUtil::$endpoint . "jgm/requerimientos/id/".$id;
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function deleteRequerimientosById($id){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('DELETE',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/".$id);
        //return $_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/id/".$id;
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                $this->saveLog('requerimiento_eliminado');
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }


    public function updateRequerimientoById($id,$introduccion, $idDiputado, $idBloque){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('PUT', $_ENV['ENDPOINT_STIRAPI'] ."jgm/requerimientos/update", [
            'form_params' => [
                'id' => $id,
                'introduccion' => $introduccion,
                'id_bloque'=> $idBloque,
                'id_diputado'=> $idDiputado
            ]
        ]);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            $this->saveLog('requerimiento_actualizado');
            return $j;
            if($j["success"]){
                $this->saveLog('requerimiento_actualizado');
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function getPreguntaById($id){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/preguntas/id/".$id);
        //return $_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/id/".$id;
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando la pregunta");

    }

    public function deletePreguntaById($id){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('DELETE',$_ENV['ENDPOINT_STIRAPI'] . "jgm/preguntas/".$id);
        //return $_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/id/".$id;
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){

                $this->saveLog('pregunta_borrada');
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando la pregunta");

    }


    public function updatePreguntaById($id,$texto){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('PUT', $_ENV['ENDPOINT_STIRAPI'] ."jgm/preguntas/update/".$id, [
            'form_params' => [
                'id' => $id,
                'texto' => $texto
            ]
        ]);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){

                $this->saveLog('pregunta_actualizada');
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando la pregunta");

    }

    public function getRequerimientosByDiputadoInformeActivo($cuilDiputado){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
    
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/diputado/".$cuilDiputado.'/informe-actual');

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }
    
    public function getRequerimientosByUser($user){

        $guid = $user["guid"];

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/".$guid);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();

            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }
    
    public function getRequerimientosByBloqueInformeActivo($idBloque){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] ."jgm/requerimientos/bloque/".$idBloque."/informe-actual");

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function getRequerimientosByInterbloqueInformeActivo($idInterbloque){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] ."jgm/requerimientos/interbloque/".$idInterbloque."/informe-actual");
        //var_dump($_ENV['ENDPOINT_STIRAPI'] ."jgm/requerimientos/interbloque/".$idInterbloque."/informe-actual");
        //die("");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }
        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function addPreguntaInRequerimiento($req){

        $d = null;

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/preguntas/add", [ 'json' => $req]);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){

                //$this->saveLog('requerimiento_actualizado');
                return $this->returnMensaje('success', "Pregunta guardada extisosamente.");
            }
        }
        return $this->returnMensaje("error","Error actualizando");

    }


    
    public function getPreguntasByRequerimiento($idRequerimiento){

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET', $_ENV['ENDPOINT_STIRAPI'] . "jgm/preguntas/requerimiento/".$idRequerimiento);
        //return $_ENV['ENDPOINT_STIRAPI'] . "jgm/preguntas/requerimiento/".$idInforme;
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                $d = $j["message"];
            }
        }
        return $d;
    }

    public function getExtraByUser($user){

        $guid = $user;//$user["guid"];

        $r = null;
        //die($_ENV['ENDPOINT_STIRAPI'] ."jgm/extras/".$guid);
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        //die($_ENV['ENDPOINT_STIRAPI'] . "jgm/extras/".$guid);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/extras/".$guid);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }
        
        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");
    }

    function getEnvioRequerimientoByUser($user){

        $role = $user["role"];

        if($role != $_ENV['ROL_RESPONSABLE_BLOQUE'] && $role != $_ENV['ROL_RESPONSABLE_INTERBLOQUE']){
            return null;
        }

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = null;

        //var_dump($user["extra"]);
        //die("");
        
        if($role == $_ENV['ROL_RESPONSABLE_BLOQUE']){

            if($user["extra"]["bloque"]["interbloque"]==null){
                $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/bloque/".$user["extra"]["bloque"]["id"]);
            }else{
                $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/interbloque/".$user["extra"]["interbloque"]["id"]);
            }
        }

        if($role == $_ENV['ROL_RESPONSABLE_INTERBLOQUE']){
            $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/interbloque/".$user["extra"]["interbloque"]["id"]);
        }

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");

    }

    function exportarRequerimientos(){

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('POST',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/exportar");

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el bloque");

    }

    function getJsonExportado(){

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/exportados/last");

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return json_encode($j["message"]);
            }
            return $j;
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el bloque");

    }

    function getArryPHPJsonExportado(){

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/exportados/last");

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
            return $j;
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el bloque");

    }

    function getEnvioRequerimientoByBloque($id){

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/bloque/".$id);
        //die($_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/bloque/".$id);
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el bloque");

    }

    function getEnvioRequerimientoByInterbloque($id){

        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/interbloque/".$id);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el bloque");

    }
    
    public function getExtraByBloque($id_bloque){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "get/extra/bloque/".$id_bloque);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            //die($j);
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }
        
        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");
    }

    public function updateExtras(){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["Authorization" => $_ENV['APIKEY_ROLESTACK']]]);

        if($_ENV["JGM_DEBUG"]){
            return [];
        }
        $res = $client->request('GET',$_ENV['ENDPOINT_ROLESTACK'] . "apps/".self::$app_id."/users");

        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){

                $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

                foreach ($j["data"] as $user){
                    $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/extras/update/info/user", [ 'json' => $user ]);
                }

                return $this->returnMensaje("ok","Actualizados correctamente");
            }

        }

    }

    public function updateExtra($req){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/extras/update/info/user", [ 'json' => $req ]);


        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $this->returnMensaje("ok","Actualizados correctamente");
            }

        }

    }


    public function getExtras(){

        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "jgm/extras/all");

        if($res->getStatusCode()==200) {
            $j = $res->getBody();

            $j = json_decode($j, true);
            if($j["success"]){
                return $j["message"];
            }
        }

        return $this->returnMensaje("error","Ocurrió un error buscando los requerimientos");

    }

    public function editExtra($req){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        
        //die(json_encode($req));
        
        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/extras/update", [
            'json' => $req
        ]);

        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $this->returnMensaje('success', "Usuario actualizado correctamente");
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',"Ocurrió un error");
        }

        return $this->returnMensaje('error', "Ocurrió un error");

    }

    public function setFechaFinInformeInterbloque($req){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/interbloques/fechafin", [
            'json' => $req
        ]);


        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){

                $this->saveLog('set_fecha_fin_interbloque');
                return $this->returnMensaje('success', $j["message"]);
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);

    }

    public function setFechaFinInformeBloque($req){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/bloques/fechafin", [
            'json' => $req
        ]);


        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){

                $this->saveLog('set_fecha_fin_informe_bloque');
                return $this->returnMensaje('success', $j["message"]);
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);

    }

    public function enviarRequerimientos($req){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        //die(json_encode($req));

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/enviar", [
            'json' => $req
        ]);

        //var_dump($res);
        //die("");

        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);


            if($j["success"]){
                $this->saveLog('envio_requerimiento');
                return $this->returnMensaje('success', $j["message"]);
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);

    }

    public function validarRequerimientos($req, $tipoValidacion){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = null;

        if($tipoValidacion=='bloque'){
            $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/bloque/validar", [
                'json' => $req
            ]);
        }elseif($tipoValidacion=='interbloque'){

            $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/requerimientos/interbloque/validar", [
                'json' => $req
            ]);
        }

        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){

                $this->saveLog('requerimiento_validado');
                return $this->returnMensaje('success', $j["message"]);
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);

    }

    public function agregarExpediente($req, $tipoValidacion){

        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = null;

        //die(json_encode($req));

        $res = $client->post($_ENV['ENDPOINT_STIRAPI'] . "jgm/envio-requerimientos/add/expediente", [
            'json' => $req
        ]);

        if($res->getStatusCode()==200) {

            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $this->returnMensaje('success', $j["message"]);
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);

    }
    
    public function getRolesByCuil(){
        
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["Authorization" => $_ENV['APIKEY_ROLESTACK']]]);
        if($_ENV["JGM_DEBUG"]){
            return [];
        }
        $res = $client->request('GET',$_ENV['ENDPOINT_ROLESTACK'] . "apps/".ApiUtil::$app_id."/users");
        
        if($res->getStatusCode()==200) {
            
            $this->getJGMUsers();
            
            $j = $res->getBody();
            $j = json_decode($j, true);
            
            if($j["success"]){
                
                $arrUsers = array();
                
                foreach ($j["data"] as $user){
                    $arrUsers[sizeof($arrUsers)] = array("cuil"=>$user["cuil"],
                                                            "rol"=>$user["role"]);
                }
                
                return $arrUsers;
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);
    }
    
    public function getDiputadoByCuil($cuil){
        
        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "diputados/cuil/".$cuil);
        
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            
            if($j["success"]){
               return $j["message"];
            }
        }
        
        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");
        
        
    }

    public function getDiputadosByBloque($bloque){

        $r = null;
        $id = $bloque["id"];
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "diputados/activos/bloque/".$id);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j;
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");


    }

    public function getDiputadosByInterbloque($interbloque){

        $r = null;
        $id = $interbloque["id"];
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);

        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "diputados/activos/interbloque/".$id);

        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            return $j;
        }

        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");


    }
    
    public function getDiputadoByAsesor($cuil){
        
        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["X-API-KEY" => $_ENV['APIKEY_STIRAPI']]]);
        
        $res = $client->request('GET',$_ENV['ENDPOINT_STIRAPI'] . "diputados/cuil/".$cuil);
        
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                return $j["message"];
            }
        }
        
        return $this->returnMensaje("error","Ocurrió un error buscando el usuario");
        
        
    }
    
    public function getRoleByGuid($guid){
        
        if(!$guid){
            die("Ocurrió un error: usuario no encontrado.");
        }
        
        $d = null;

        if($_ENV["JGM_DEBUG"]){
            return [];
        }

        $client = new \GuzzleHttp\Client(["headers" => ["Authorization" => $_ENV['APIKEY_ROLESTACK']]]);
        
        $res = $client->request('GET',$_ENV['ENDPOINT_ROLESTACK']. "users/".$guid."/".ApiUtil::$app_id."/role");
        //die(ApiUtil::$endpoint_rolestack . "users/".$guid."/".ApiUtil::$app_id."/role");
        
        if($res->getStatusCode()==200) {
            
            $j = $res->getBody();
            $j = json_decode($j, true);
            
            if($j["success"]){
                return $j["data"]["role"];
            }

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);
        
    }
    
    public function getJGMUsers(){
        
        $d = null;
        $client = new \GuzzleHttp\Client(["headers" => ["Authorization" => $_ENV['APIKEY_ROLESTACK']]]);
        if($_ENV["JGM_DEBUG"]){
            return [];
        }
        $res = $client->request('GET',$_ENV['ENDPOINT_ROLESTACK'] . "apps/".self::$app_id."/users");
        //die(ApiUtil::$endpoint_rolestack . "apps/".self::$app_id."/users");
        if($res->getStatusCode()==200) {
            
            $j = $res->getBody();
            $j = json_decode($j, true);

            if($j["success"]){
                
                $arrUsers = array();
                
                foreach ($j["data"] as $user){

                        $rol = $user["role"];

                        $arrUsers[sizeof($arrUsers)] = array("cuil"=>$user["cuil"],
                                                                "guid"=>$user["_id"],
                                                                "nombre"=>$user["first_name"],
                                                                "apellido"=>$user["last_name"],
                                                                "rol"=>$rol);
                    }
                }

                return $arrUsers;

        }else{
            $j = $res->getBody();
            return $this->returnMensaje('error',$j["message"]);
        }

        return $this->returnMensaje('error', $j["message"]);
    }

    private static function returnMensaje($tipo,$mensaje){

        $success = false;

        if($tipo=="success"){
            $success = true;
        }

        return array("success"=>$success,
                        "mensaje"=>$mensaje);
    }

    private function saveLog($accion){

        $user = $this->session->get('user');

        $informe = $this->session->get('informe');
        $diputado = null;

        if($user["extra"]["diputado"]["apellido"] != null){
            $diputado = $user["extra"]["diputado"]["apellido"].", ".$user["extra"]["diputado"]["nombre"];
        }

        $bloque = $user["extra"]["bloque"]["nombre"];
        $interbloque = $user["extra"]["interbloque"]["nombre"];
        $nombre = $user["apellido"].", ".$user["nombre"];

        $newLog = new JgmLog();

        $newLog->setIp($this->request->getClientIp());
        $newLog->setRole($user["role"]);
        $newLog->setUser($nombre);
        $newLog->setCuil($user["cuil"]);
        $newLog->setOperacion($accion);

        $newLog->setDiputado($diputado);
        $newLog->setBloque($bloque);
        $newLog->setInterbloque($interbloque);

        $newLog->setInforme($informe["numero"]);
        $newLog->setFecha(new \DateTime());
        $newLog->setGuid($user["guid"]);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($newLog);
        $em->flush();

    }

}