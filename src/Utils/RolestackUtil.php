<?php

namespace App\Utils;

class RolestackUtil
{
    public static $endpoint = "http://10.2.86.219:4000/";
    public static $apiKey = "dse.91295A27F6216C8DB98C969D4FAD8";

    public static function getRoleByUser($app, $userId){
        $r = null;
        $client = new \GuzzleHttp\Client(["headers" => ["Authorization" => RolestackUtil::$apiKey]]);
        $res = $client->request('GET', RolestackUtil::$endpoint . "users/" . $userId . "/" . $app . "/role");
        if($res->getStatusCode()==200) {
            $j = $res->getBody();
            $j = json_decode($j, true);
            if($j["success"]){
                $r = $j["data"]["role"];
            }
        }
        return $r;
    }

    public static function getUsers(){

        $client = $client = new \GuzzleHttp\Client(["headers" => ["Authorization" => RolestackUtil::$apiKey]]);

        $response = $client->get('http://10.2.86.219:4000/apps/infojgm/users');
        $apps = $response->getBody()->getContents();
        $jd = json_decode($apps,true);
        if($jd["success"]==false){
            throw new \Exception("Error al obtener los usuarios");
        }
        return $jd["data"];
    }
}