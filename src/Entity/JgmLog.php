<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="infojgm_log")
 * @ORM\Entity(repositoryClass="App\Repository\JgmLogRepository")
 */
class JgmLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $operacion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="text")
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     */
    private $ip;

    /**
     * @ORM\Column(type="text")
     */
    private $role;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $diputado;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $bloque;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $interbloque;

    /**
     * @ORM\Column(type="integer")
     */
    private $informe;

    /**
     * @ORM\Column(type="text")
     */
    private $guid;

    /**
     * @ORM\Column(type="text")
     */
    private $cuil;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOperacion(): ?string
    {
        return $this->operacion;
    }

    public function setOperacion(string $operacion): self
    {
        $this->operacion = $operacion;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getDiputado(): ?string
    {
        return $this->diputado;
    }

    public function setDiputado(?string $diputado): self
    {
        $this->diputado = $diputado;

        return $this;
    }

    public function getBloque(): ?string
    {
        return $this->bloque;
    }

    public function setBloque(?string $bloque): self
    {
        $this->bloque = $bloque;

        return $this;
    }

    public function getInterbloque(): ?string
    {
        return $this->interbloque;
    }

    public function setInterbloque(?string $interbloque): self
    {
        $this->interbloque = $interbloque;

        return $this;
    }

    public function getInforme(): ?int
    {
        return $this->informe;
    }

    public function setInforme(int $informe): self
    {
        $this->informe = $informe;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getCuil(): ?string
    {
        return $this->cuil;
    }

    public function setCuil(string $cuil): self
    {
        $this->cuil = $cuil;

        return $this;
    }


}
