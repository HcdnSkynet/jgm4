<?php

namespace App\EventListener;

use App\Utils\ApiUtil;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use DateTime;

class KernelControllerListener
{
    private $log = [];
    private $container;
    private $router;
    private $session;
    private $twig;
    private $apiUtil;
    public $user;
    public $informe;
    public $em;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->em = $container->get('doctrine')->getManager();
        $this->session = $container->get('session');
        $this->twig = $container->get('twig');
        $this->apiUtil = new ApiUtil($this->session, $this->container->get('request_stack')->getCurrentRequest(),
                                        $this->container);
    }

    public function onKernelController(GetResponseEvent $event)
    {
        try{
        $hcdnUser = $this->session->get('hcdn-user');
        $debug = $_ENV['JGM_DEBUG'];
        //die($event->getRequest()->get('_route'));

        if (!$hcdnUser) {
            $route_connect = 'hcdn-connect';
            $route_callback = 'hcdn-callback';
            $route_logout = 'logout';

            if ($route_callback === $event->getRequest()->get('_route') or
                $event->getRequest()->get('_route') === $route_connect or
                $event->getRequest()->get('_route') === $route_logout) {
                return;
            } else {
                //die("Sin hcdnuser");
                $url = $this->router->generate('hcdn-connect');
                $event->setResponse(new RedirectResponse($url));
            }

        }

        if($debug){
                switch ($_ENV["DEBUG_USER_SELECTED"]){
                case "admin":
                    $this->user["role"] = "admin";
                    $this->user["apellido"] = "Admin";
                    $this->user["nombre"] = "Admin";
                    $this->user["guid"] = "d185c9f8-9bb4-4ef1-a28b-30d5e09bba43";
                    $this->user["cuil"] = "1";
                    break;
                case "direccion_secretaria":
                    $this->user["role"] = "direccion_secretaria";
                    $this->user["apellido"] = "dir_sec";
                    $this->user["nombre"] = "dir_sec";
                    $this->user["guid"] = "0d1448e0-3f88-447e-8d0b-4149235d8ae4";
                    $this->user["cuil"] = "2";
                    break;
                case "pregjefegabinete01@hcdn.gob.ar":
                    $this->user["role"] = "responsable_interbloque";
                    $this->user["apellido"] = "Resp interbloque";
                    $this->user["nombre"] = "Resp interbloque";
                    $this->user["guid"] = "d15a6c44-fbd5-4bd4-9d7c-66a27ff73850";
                    $this->user["cuil"] = "3";
                    break;
                case "pregjefegabinete02@hcdn.gob.ar":
                    $this->user["role"] = "responsable_bloque";
                    $this->user["apellido"] = "Resp bloque";
                    $this->user["nombre"] = "Resp bloque";
                    $this->user["guid"] = "e9cb9505-fbe3-4e8b-b88b-c585640f667d";
                    $this->user["cuil"] = "4";
                    break;
                case "pregjefegabinete03@hcdn.gob.ar":
                    $this->user["role"] = "personal_parlamentario";
                    $this->user["apellido"] = "pparl";
                    $this->user["nombre"] = "pparl";
                    $this->user["guid"] = "63b73e1c-6c9a-402c-94e3-16985f82011f";
                    $this->user["cuil"] = "5";
                    break;
                case "pregjefegabinete04@hcdn.gob.ar":
                    $this->user["role"] = "asesor";
                    $this->user["apellido"] = "asesor";
                    $this->user["nombre"] = "asesor";
                    $this->user["guid"] = "98e70e9d-d7a3-45a5-9be5-0874646f1a04";
                    $this->user["cuil"] = "6";
                    break;
                case "pregjefegabinete05@hcdn.gob.ar":
                    $this->user["role"] = "responsable_bloque";
                    $this->user["apellido"] = "Resp bloque";
                    $this->user["nombre"] = "Resp bloque";
                    $this->user["guid"] = "4edc7a95-0e78-42b3-9d61-43469bbe7f37";
                    $this->user["cuil"] = "7";
                    break;
                case "pregjefegabinete06@hcdn.gob.ar":
                    $this->user["role"] = "personal_parlamentario";
                    $this->user["role"] = "personal_parlamentario";
                    $this->user["apellido"] = "pparl";
                    $this->user["nombre"] = "pparl";
                    $this->user["guid"] = "7a893f3a-365a-4609-b2a7-7580b9699ff4";
                    $this->user["cuil"] = "8";
                    break;
                case "pregjefegabinete07@hcdn.gob.ar":
                    $this->user["role"] = "asesor";
                    $this->user["apellido"] = "asesor";
                    $this->user["nombre"] = "asesor";
                    $this->user["guid"] = "078fb44c-e6fc-476a-8da2-6db7edd5473b";
                    $this->user["cuil"] = "9";
                    break;
                case "pregjefegabinete08@hcdn.gob.ar":
                    $this->user["role"] = "responsable_bloque";
                    $this->user["apellido"] = "Resp bloque";
                    $this->user["nombre"] = "Resp bloque";
                    $this->user["guid"] = "42fa636a-dfb6-474e-ab71-b478ba8c0d0f";
                    $this->user["cuil"] = "10";
                    break;
                case "pregjefegabinete09@hcdn.gob.ar":
                    $this->user["role"] = "personal_parlamentario";
                    $this->user["role"] = "personal_parlamentario";
                    $this->user["role"] = "personal_parlamentario";
                    $this->user["guid"] = "35a78ae8-3c67-4fc9-bc73-ecf2d005c4f3";
                    $this->user["cuil"] = "11";
                    break;
                case "pregjefegabinete10@hcdn.gob.ar":
                    $this->user["role"] = "asesor";
                    $this->user["apellido"] = "asesor";
                    $this->user["nombre"] = "asesor";
                    $this->user["guid"] = "bc12cf24-7f0f-4566-b44a-68dcef9e0546";
                    $this->user["cuil"] = "12";
                    break;

                default:
                    die("Debug fallido");

            }
        }else{
            $this->user = $this->session->get('user');
        }


        if(is_array($this->user["role"])){
            die("El usuario no tiene un rol asignado");
        }

        //var_dump($this->user);
        //die("");

        $isAllowed = $this->isActionAllowedByRole($event->getRequest()->get('_route') , $this->user["role"]);
        //die($isAllowed."");
        if(!$isAllowed){
            die("Rol no autorizado para realizar esta acción.");
            //return;
            //$route_logout = 'logout';
            //$event->setResponse(new RedirectResponse($route_logout));
        }

        if($this->user["guid"]){
            $this->user["extra"] = $this->apiUtil->getExtraByUser($this->user["guid"]);
        }else{
            $route_connect = 'hcdn-connect';
            $route_callback = 'hcdn-callback';
            $route_logout = 'logout';
            if ($route_callback === $event->getRequest()->get('_route') or
                $event->getRequest()->get('_route') === $route_connect or
                $event->getRequest()->get('_route') === $route_logout) {
                return;
            } else {
                $url = $this->router->generate('hcdn-connect');
                $event->setResponse(new RedirectResponse($url));
            }
        }

        //die(var_dump($this->user));

        $informe = $this->apiUtil->getUltimoInforme();
        $estadoInforme = null;

        $this->checkAsociacionPorRolCorrecta($this->user);

        if($this->user["extra"]["bloque"] != null
            && $this->user["extra"]["bloque"]["cierreBloque"]
            && $this->user["role"] != $_ENV['ROL_RESPONSABLE_BLOQUE']){
            $estadoInforme = $this->user["extra"]["bloque"]["cierreBloque"]["estado"]["nombre"];
            $informe["fechaFin"] = $this->user["extra"]["bloque"]["cierreBloque"]["fechaCierre"];
        }else{
            $envioRequerimiento = $this->apiUtil->getEnvioRequerimientoByUser($this->user);

            if($envioRequerimiento!=null){
                $estadoInforme = 'validando';
            }else{
                $estadoInforme = $informe["estado"]["nombre"];
            }
        }

        $this->session->set("informe", $informe);
        $this->session->set("user", $this->user);
        $this->session->set('hcdn-user',$this->user);

        $this->informe = $this->session->get('informe');

        //TODO descomentar
        $historiales = [];//$this->apiUtil->getInformesCerrados();

        $this->twig->addGlobal('estado_informe', $estadoInforme);
        $this->twig->addGlobal('historial_informes', $historiales);
        $this->twig->addGlobal('user', $this->user);
        $this->twig->addGlobal('informe', $this->informe);
        //die(var_dump($this->session->get("user")));
        }catch (\Exception $e){
            die("sddsaasd");
        }
    }

    private function checkAsociacionPorRolCorrecta($user)
    {

        $role = $user["role"];
        $extra = $user["extra"];

        switch ($role) {
            case $_ENV['ROL_DIPUTADO']:
                //$diputados = $apiUtils->getDiputadoByCuil($this->get('session')->get('user')["cuil"]);
                break;
            case $_ENV['ROL_ASESOR']:
                if ($extra["diputado"] == null) {
                    die("Es mandatorio que el rol ASESOR tenga un diputado asociado");
                }
                break;
            case $_ENV['ROL_PERSONAL_PARLAMENTARIO']:
                if ($extra["bloque"] == null) {
                    die("Es mandatorio que el rol PERSONAL PARLAMENTARIO tenga un bloque asociado");
                }
                break;
            case $_ENV['ROL_RESPONSABLE_BLOQUE']:
                if ($extra["bloque"] == null) {
                    die("Es mandatorio que el rol RESPONSABLE DE BLOQUE tenga un bloque asociado");
                }
                break;
            case $_ENV['ROL_RESPONSABLE_INTERBLOQUE']:
                if ($extra["interbloque"] == null) {
                    die("Es mandatorio que el rol RESPONSABLE DE INTERBLOQUE tenga un interbloque asociado");
                };
                break;
            case $_ENV['ROL_ADMIN']:
                //a
                break;
            case $_ENV['ROL_DIRECCION_SECRETARIA']:
                //no hago nada
                break;
        }

        //print_r($extra);
        //die('<br>Rol: '.$role);

    }

    private function isActionAllowedByRole($route ,$role){

        $isAllowed = false;

        //var_dump($route);

        if($route == '_informacion_general' ||
            $route == '_normativa' ||
            $route == '_soporte' ||
            $route == '_index' ||
            $route == 'hcdn-connect' ||
            $route == 'hcdn-callback' ||
            $route == '_log_out' ||
            $route == null ){
            $isAllowed = true;
            //var_dump($isAllowed);
            return $isAllowed;
        }


        if($role==$_ENV['ROL_DIRECCION_SECRETARIA']){

            if(
                $route==='_informe' ||
                $route==='_validar_requerimientos' ||
                $route==='_validar_requerimientos_post' ||
                $route==='_save_informe' ||
                $route==='_delete_informe' ||
                $route==='_add_expediente_to_envio_req' ||
                $route==='_exportar_requerimientos' ||
                $route==='_json_exportado_last' ||
                $route==='_excel_exportado_last' ||
                $route==='_check_todo_validado' ||
                $route==='get_preguntas_by_requerimiento' ||
                $route==='get_requerimiento_by_id' ||
                $route==='pdf_requerimientos_secretaria'){
                $isAllowed = true;
            }
        }

        if($role==$_ENV['ROL_RESPONSABLE_INTERBLOQUE']){
            if(
                //$route==='_listar_requerimiento_actual' ||
                //$route==='_listar_requerimiento_actual_bloque' ||
                //$route==='_validar_requerimientos_post' ||
                $route==='_listar_requerimiento_actual_interbloque' ||
                $route==='pdf_requerimientos_interbloque' ||
                //$route==='_listar_requerimiento_usuario' ||
                //$route==='_admin_panel' ||
                //$route==='_estado_envio'||
                //$route==='_informe' ||
                $route==='_set_fecha_fin' ||
                //$route==='_set_fecha_fin_interbloque' ||
                $route==='_enviar_requerimientos' ||
                //$route==='_validar_requerimientos' ||
                //$route==='_migrar' ||
                //$route==='ajax-update-user' ||
                //$route==='hcdn-connect' ||
                //$route==='hcdn-callback' ||
                //$route==='_save_informe' ||
                //$route==='_delete_informe' ||
                $route==='add_pregunta_by_req_id' ||
                //$route==='_save_fecha_fin_interbloque' ||
                $route==='_save_fecha_fin_bloque' ||
                $route==='_enviar_req_post' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_add_expediente_to_envio_req' ||
                //$route==='_exportar_requerimientos' ||
                //$route==='_json_exportado_last' ||
                //$route==='_excel_exportado_last' ||
                //$route==='_check_todo_validado' ||
                $route==='saveRequerimiento' ||
                //$route==='edit_extra' ||
                $route==='get_preguntas_by_requerimiento' ||
                $route==='get_requerimiento_by_id' ||
                $route==='delete_requerimiento_by_id' ||
                $route==='update_requerimiento_by_id' ||
                $route==='get_pregunta_by_id' ||
                $route==='delete_pregunta_by_id' ||
                $route==='update_pregunta_by_id' ||

                $route==='pdf_requerimientos' ||
                $route==='pdf_requerimientos_bloque' ||
                $route==='pdf_requerimientos_secretaria' ||
                $route==='pdf_envio_requerimientos'  ||
                $route==='_listar_historial_informe'
                ){
                $isAllowed = true;
            }
        }

        if($role==$_ENV['ROL_RESPONSABLE_BLOQUE']){
            if(
                $route==='_crear_requerimiento' ||
                //$route==='_listar_requerimiento_actual' ||
                $route==='_listar_requerimiento_actual_bloque' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_listar_requerimiento_actual_interbloque' ||
                //$route==='_listar_requerimiento_usuario' ||
                //$route==='_admin_panel' ||
                //$route==='_estado_envio'||
                //$route==='_informe' ||
                $route==='_set_fecha_fin' ||
                //$route==='_set_fecha_fin_interbloque' ||
                $route==='_enviar_requerimientos' ||
                //$route==='_validar_requerimientos' ||
                //$route==='_migrar' ||
                //$route==='ajax-update-user' ||
                //$route==='hcdn-connect' ||
                //$route==='hcdn-callback' ||
                //$route==='_save_informe' ||
                //$route==='_delete_informe' ||
                $route==='add_pregunta_by_req_id' ||
                //$route==='_save_fecha_fin_interbloque' ||
                $route==='_save_fecha_fin_bloque' ||
                $route==='_enviar_req_post' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_add_expediente_to_envio_req' ||
                //$route==='_exportar_requerimientos' ||
                //$route==='_json_exportado_last' ||
                //$route==='_excel_exportado_last' ||
                //$route==='_check_todo_validado' ||
                $route==='saveRequerimiento' ||
                //$route==='edit_extra' ||
                $route==='get_preguntas_by_requerimiento' ||
                $route==='get_requerimiento_by_id' ||
                $route==='delete_requerimiento_by_id' ||
                $route==='update_requerimiento_by_id' ||
                $route==='get_pregunta_by_id' ||
                $route==='delete_pregunta_by_id' ||
                $route==='update_pregunta_by_id' ||

                $route==='pdf_requerimientos' ||
                $route==='pdf_requerimientos_bloque' ||
                $route==='pdf_requerimientos_secretaria' ||
                $route==='pdf_envio_requerimientos'||
                $route==='_listar_historial_informe'  ){
                $isAllowed = true;
            }
        }

        if($role==$_ENV['ROL_ADMIN']){
            if(
                //$route==='_crear_requerimiento' ||
                //$route==='_listar_requerimiento_actual' ||
                //$route==='_listar_requerimiento_actual_bloque' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_listar_requerimiento_actual_interbloque' ||
                //$route==='_listar_requerimiento_usuario' ||
                $route==='_admin_panel' ||
                //$route==='_estado_envio'||
                //$route==='_informe' ||
                //$route==='_set_fecha_fin' ||
                //$route==='_set_fecha_fin_interbloque' ||
                //$route==='_enviar_requerimientos' ||
                //$route==='_validar_requerimientos' ||
                $route==='_migrar' ||
                //$route==='ajax-update-user' ||
                //$route==='hcdn-connect' ||
                //$route==='hcdn-callback' ||
                //$route==='_save_informe' ||
                //$route==='_delete_informe' ||
                //$route==='add_pregunta_by_req_id' ||
                //$route==='_save_fecha_fin_interbloque' ||
                //$route==='_save_fecha_fin_bloque' ||
                //$route==='_enviar_req_post' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_add_expediente_to_envio_req' ||
                //$route==='_exportar_requerimientos' ||
                //$route==='_json_exportado_last' ||
                //$route==='_excel_exportado_last' ||
                //$route==='_check_todo_validado' ||
                //$route==='saveRequerimiento' ||
                $route==='edit_extra' ||
                //$route==='get_preguntas_by_requerimiento' ||
                //$route==='get_requerimiento_by_id' ||
                //$route==='delete_requerimiento_by_id' ||
                //$route==='update_requerimiento_by_id' ||
                //$route==='get_pregunta_by_id' ||
                //$route==='delete_pregunta_by_id' ||
                //route==='update_pregunta_by_id' ||

                $route==='pdf_requerimientos' ||
                $route==='pdf_requerimientos_bloque' ||
                $route==='pdf_requerimientos_secretaria' ||
                $route==='pdf_envio_requerimientos'  ){
                $isAllowed = true;
            }
        }

        if($role==$_ENV['ROL_PERSONAL_PARLAMENTARIO']){
            if(
                $route==='_crear_requerimiento' ||
                $route==='_listar_requerimiento_actual' ||
                $route==='_listar_requerimiento_actual_bloque' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_listar_requerimiento_actual_interbloque' ||
                //$route==='_listar_requerimiento_usuario' ||
                //$route==='_admin_panel' ||
                //$route==='_estado_envio'||
                //$route==='_informe' ||
                //$route==='_set_fecha_fin' ||
                //$route==='_set_fecha_fin_interbloque' ||
                //$route==='_enviar_requerimientos' ||
                //$route==='_validar_requerimientos' ||
                //$route==='_migrar' ||
                //$route==='ajax-update-user' ||
                //$route==='hcdn-connect' ||
                //$route==='hcdn-callback' ||
                $route==='_save_informe' ||
                //$route==='_delete_informe' ||
                $route==='add_pregunta_by_req_id' ||
                //$route==='_save_fecha_fin_interbloque' ||
                //$route==='_save_fecha_fin_bloque' ||
                //$route==='_enviar_req_post' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_add_expediente_to_envio_req' ||
                //$route==='_exportar_requerimientos' ||
                //$route==='_json_exportado_last' ||
                //$route==='_excel_exportado_last' ||
                //$route==='_check_todo_validado' ||
                $route==='saveRequerimiento' ||
                $route==='edit_extra' ||
                $route==='get_preguntas_by_requerimiento' ||
                $route==='get_requerimiento_by_id' ||
                $route==='delete_requerimiento_by_id' ||
                $route==='update_requerimiento_by_id' ||
                $route==='get_pregunta_by_id' ||
                $route==='delete_pregunta_by_id' ||
                $route==='update_pregunta_by_id' ||

                $route==='pdf_requerimientos' ||
                $route==='pdf_requerimientos_bloque' ||
                $route==='pdf_requerimientos_secretaria' ||
                $route==='pdf_envio_requerimientos' ||
                $route==='_listar_historial_informe' ){
                $isAllowed = true;
            }
        }

        if($role==$_ENV['ROL_ASESOR']){
            if(
                $route==='_crear_requerimiento' ||
                $route==='_listar_requerimiento_actual' ||
                $route==='_listar_requerimiento_actual_bloque' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_listar_requerimiento_actual_interbloque' ||
                //$route==='_listar_requerimiento_usuario' ||
                //$route==='_admin_panel' ||
                //$route==='_estado_envio'||
                //$route==='_informe' ||
                //$route==='_set_fecha_fin' ||
                //$route==='_set_fecha_fin_interbloque' ||
                //$route==='_enviar_requerimientos' ||
                //$route==='_validar_requerimientos' ||
                //$route==='_migrar' ||
                //$route==='ajax-update-user' ||
                //$route==='hcdn-connect' ||
                //$route==='hcdn-callback' ||
                $route==='_save_informe' ||
                //$route==='_delete_informe' ||
                $route==='add_pregunta_by_req_id' ||
                //$route==='_save_fecha_fin_interbloque' ||
                //$route==='_save_fecha_fin_bloque' ||
                //$route==='_enviar_req_post' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_add_expediente_to_envio_req' ||
                //$route==='_exportar_requerimientos' ||
                //$route==='_json_exportado_last' ||
                //$route==='_excel_exportado_last' ||
                //$route==='_check_todo_validado' ||
                $route==='saveRequerimiento' ||
                $route==='edit_extra' ||
                $route==='get_preguntas_by_requerimiento' ||
                $route==='get_requerimiento_by_id' ||
                $route==='delete_requerimiento_by_id' ||
                $route==='update_requerimiento_by_id' ||
                $route==='get_pregunta_by_id' ||
                $route==='delete_pregunta_by_id' ||
                $route==='update_pregunta_by_id' ||

                $route==='pdf_requerimientos' ||
                $route==='pdf_requerimientos_bloque' ||
                $route==='pdf_requerimientos_secretaria' ||
                $route==='pdf_envio_requerimientos' ||
                $route==='_listar_historial_informe' ){
                $isAllowed = true;
            }
        }

        if($role==$_ENV['ROL_DIPUTADO']){
            if(
                $route==='_crear_requerimiento' ||
                $route==='_listar_requerimiento_actual' ||
                $route==='_listar_requerimiento_actual_bloque' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_listar_requerimiento_actual_interbloque' ||
                //$route==='_listar_requerimiento_usuario' ||
                //$route==='_admin_panel' ||
                //$route==='_estado_envio'||
                //$route==='_informe' ||
                //$route==='_set_fecha_fin' ||
                //$route==='_set_fecha_fin_interbloque' ||
                //$route==='_enviar_requerimientos' ||
                //$route==='_validar_requerimientos' ||
                //$route==='_migrar' ||
                //$route==='ajax-update-user' ||
                //$route==='hcdn-connect' ||
                //$route==='hcdn-callback' ||
                $route==='_save_informe' ||
                //$route==='_delete_informe' ||
                $route==='add_pregunta_by_req_id' ||
                //$route==='_save_fecha_fin_interbloque' ||
                //$route==='_save_fecha_fin_bloque' ||
                //$route==='_enviar_req_post' ||
                //$route==='_validar_requerimientos_post' ||
                //$route==='_add_expediente_to_envio_req' ||
                //$route==='_exportar_requerimientos' ||
                //$route==='_json_exportado_last' ||
                //$route==='_excel_exportado_last' ||
                //$route==='_check_todo_validado' ||
                $route==='saveRequerimiento' ||
                $route==='edit_extra' ||
                $route==='get_preguntas_by_requerimiento' ||
                $route==='get_requerimiento_by_id' ||
                $route==='delete_requerimiento_by_id' ||
                $route==='update_requerimiento_by_id' ||
                $route==='get_pregunta_by_id' ||
                $route==='delete_pregunta_by_id' ||
                $route==='update_pregunta_by_id' ||

                $route==='pdf_requerimientos' ||
                $route==='pdf_requerimientos_bloque' ||
                $route==='pdf_requerimientos_secretaria' ||
                $route==='pdf_envio_requerimientos' ||
                $route==='_listar_historial_informe' ){
                $isAllowed = true;
            }
        }


        //var_dump($isAllowed);

        return $isAllowed;
    }
}