<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManager;
use AppBundle\Utils\RolestackUtil;
use Symfony\Component\HttpFoundation\Request;
use Twig_Environment;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\DefaultController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RequestListener
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Twig_Environment;
     */
    private $twig;

    public function onKernelController(FilterControllerEvent $event)
    {

        $controller = $event->getController();
        $request = $event->getRequest();
        $session = $request->getSession();
        if($controller[0] instanceof DefaultController){

            die("fsadlkjafsjlkfsajlksfa");
            
            /*$hu = array();
            $hu["username"] = "esalto";
            $hu["email"] = "esalto@hcdn.gob.ar";
            $hu["first_name"] = "Enrique";
            $hu["last_name"] = "Salto";
            $hu["cuil"] = "-";
            $hu["guid"] = "esalto123";
            $hu["description"] = "Enrique";
            $session->set("hcdn-user", $hu);*/
            if(!$session->has("hcdn-user")){
                /* Si no tengo datos de sesión del usuario, inicio proceso de autorización */
                $ruta = $this->container->get('router')->generate("hcdn-connect");
                $event->setController(function() use ($ruta) {
                    return new RedirectResponse($ruta);
                });
            }
        }
        if($session->has("hcdn-user")){
            $arr = $event->getRequest()->getSession()->get('hcdn-user');
            $this->twig->addGlobal('hcdn_user', $arr);
            $brole = array();
                $r = RolestackUtil::getRoleByUser("infojgm", $arr["sub"]);
            if($r==null){
                //$html = $this->container->get('templating')->render('PresenciaBundle:Default:colores.html.twig');
                //die($html.strlen($html)."|");
                $session->remove("hcdn-user");
                $session->remove("user-role");
                $session->remove("user-cuil");
                die("<!DOCTYPE html><head> <style>html, body{width: 100%;}</style></head><html><body> <div style=\"width: 100%; text-align: center;\"> <br><center> <div style=\"width: 50%;\"> <br/> Usted no tiene acceso a Gabinete.<br/> Para solicitarlo, envíe un correo a <b>soporte.dse@hcdn.gob.ar</b> con los siguientes datos: <div style=\"width: 100%; text-align: center; padding-left: 38%;\"> <ul style=\"text-align: left;\"> <li>Nombre y apellido</li><li>CUIL</li><li>Interno</li><li>Oficina</li></ul> </div>Por cualquier consulta comuníquese con el interno 3471/3483. </div></center> </div></body></html>");
            }
            $brole["name"] = $r;
            $event->getRequest()->getSession()->set('user-role',$r);
            $this->twig->addGlobal('hcdn_user', $arr);
            $this->twig->addGlobal('bos_user', $this->container->get('bos_user')->getUserByCuil($arr["cuil"]));
            $this->twig->addGlobal('bos_role', $brole);
        }
    }

    public function __construct(ContainerInterface $container,EntityManager $manager,Twig_Environment $twig){
        $this->container = $container;
        $this->em = $manager;
        $this->twig = $twig;
        //$this->helper = $this->container->get('helper');
    }

    public function onKernelRequest(GetResponseEvent $event)
    {

        die("fsadlkjafsjlkfsajlksfa");
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        try{
            die("fsadlkjafsjlkfsajlksfa");
            $this->checkPermissions($event->getRequest());
        }catch(\Exception $e){

        }
    }



    private function checkPermissions(Request $request){

        die("fsadlkjafsjlkfsajlksfa");
        $buser = $this->container->get('bos_user');
        $user = $buser->getUser();
        if($user){
            $systemName = $buser->system->getName();
            $role = $buser->getRoleByUser($systemName, $user);
            $routeName = $request->get('_route');
            $rn = $role->getName();
            $message = "No tiene permisos para acceder a esta URL";
            switch($routeName){
                case "_recinto":
                    if($rn!="admin"&&$rn!="seguridad"&&$rn!="consulta_compleja"){
                        die($message);
                    }
                    break;
                case "_modificarDip":
                    if($rn!="admin"&&$rn!="seguridad"){
                        die($message);
                    }
                    break;
                case "_modificarSec":
                    if($rn!="admin"&&$rn!="seguridad"){
                        die($message);
                    }
                    break;
                case "_presentesDip":
                    if($rn!="admin"&&$rn!="consulta_compleja"&&$rn!="seguridad"){
                        die($message);
                    }
                    break;
            }
        }
    }

}