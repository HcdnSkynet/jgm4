<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Firebase\JWT\JWT;
use App\Utils\ApiUtil;
use App\OAuth\OIDTool;

/**
 * @Route("/oauth")
 */
class HcdnAuthController extends Controller
{
    private $em = null;
    

    /**
     * @Route("/connect", name="hcdn-connect")
     */
    public function connect(Request $request)
    {
        if($_ENV["JGM_DEBUG"]){
            return $this->redirect('http://localhost/jgm4/public/index.php');
        }else{
            $oid = new OIDTool($_ENV['OID_CLIENT_ID'], //Client ID
                $_ENV['OID_AUTH_CALLBACK'], //Redirect URI
                $_ENV['OID_SECRET']); //Client Secret

            return $this->redirect($oid->getAuthorizationUrl());
        }

    }
    
    /**
     * @Route("/callback", name="hcdn-callback")
     */
    public function callback(Request $request)
    {

        $oid = new OIDTool($_ENV['OID_CLIENT_ID'], //Client ID
            $_ENV['OID_AUTH_CALLBACK'], //Redirect URI
            $_ENV['OID_SECRET']); //Client Secret
        $state = $request->get("state");
        $code = $request->get("code");
        //die("state ".$state . " code ". $code);
        if(!isset($code)||!isset($state)){
            return $this->redirect($oid->getAuthorizationUrl());
        }

        if($oid->stateIsValid($state)){
            try{
                
                $apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
                $publicKeyDir = $this->get('kernel')->getRootDir() . "/../keys/pubkey.pem";
                $user = $oid->getUserCredentials($code, $publicKeyDir);


                $request->getSession()->set('hcdn-user', $user);
                $hcdnUser = $request->getSession()->get('hcdn-user');
                $extra = $apiUtil->getExtraByUser($hcdnUser["sub"]);

                if($extra==null){

                    $cuil = $hcdnUser["cuil"];
                    $guid = $hcdnUser["sub"];
                    $role = $apiUtil->getRoleByGuid($guid);
                    $email= $hcdnUser["username"]."@hcdn.gob.ar";
                    $firstName = $hcdnUser["name"];
                    $lastName= $hcdnUser["last_name"];

                    $req = array(
                        "cuil"=>$cuil,
                        "_id"=>$guid,
                        "role"=>$role,
                        "email"=>$email,
                        "first_name"=>$firstName,
                        "last_name"=>$lastName
                    );

                    $apiUtil->editExtra($req);
                    $extra = $apiUtil->getExtraByUser($guid);

                }

                //echo "hcdnAuth";
                //var_dump($extra);
                //die("");
                if(ENV['JGM_DEBUG']){
                    $apiUtil->updateExtras();
                }

                $request->getSession()->set("user",array("nombre"=>$hcdnUser["name"],
                                                            "apellido"=>$hcdnUser["last_name"],
                                                            "cuil"=>$hcdnUser["cuil"],
                                                            "guid"=>$hcdnUser["sub"],
                                                            "role"=>$apiUtil->getRoleByGuid($hcdnUser["sub"]),
                                                            "extra"=>$extra));
            }catch(\Exception $e){
                die("Error: " . $e->getMessage());
            }
        }else{
            die("State no válido." . $state);
        }
        return new RedirectResponse($this->generateUrl('_index'));
    }
    
}