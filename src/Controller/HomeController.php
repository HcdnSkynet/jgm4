<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Utils\ApiUtil;

class HomeController extends Controller
{

    /**
    * @Route("/", name="_index")
    */
    public function index(){
        return $this->render('index.html.twig');
    }

    /**
    * @Route("/crear/requerimiento", name="_crear_requerimiento")
    */
    public function crearRequerimiento(Request $request){

        $user = $this->get('session')->get('user');
        $session =  $this->get('session');
        
        $diputadosActivos = $this->getDiputadosByRole($user["role"],$request);
        $bloquesActivos = $this->getBloquesByRole($user["role"], $request);

        $result = null;

        if($this->get('session')->get('result')){
            $result = $session->get("result");
            $session->set("result", null);
        }

        return $this->render('crearRequerimiento.html.twig',array("diputados"=>$diputadosActivos,
                                                                        "bloques"=>$bloquesActivos,
                                                                        "result"=>$result));
    }

    /**
    * @Route("/listar/requerimiento/actual", name="_listar_requerimiento_actual")
    */
    public function listarRequerimiento(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $user = $this->get('session')->get('user');
        //$requerimientos = $apiUtils->getRequerimientosByUserInformeActivo($user);
        $requerimientos = $this->getRequerimientosByRole($user["role"], $request);

        $diputadosActivos = $this->getDiputadosByRole($user["role"], $request);
        $bloquesActivos = $this->getBloquesByRole($user["role"],$request);
        $interbloques = $apiUtils->getInterbloques();
        //$requerimientosHistorial = $apiUtils->getRequerimientosByNumeroInforme(133);


        return $this->render('listarRequerimiento.html.twig',array("requerimientos"=>$requerimientos,
                                                                            "diputados"=>$diputadosActivos,
                                                                            "bloques"=>$bloquesActivos,
                                                                            "interbloques"=>$interbloques));
    }

    /**
     * @Route("/listar/historial/informe/{numero}", name="_listar_historial_informe")
     */
    public function listarHistorialInforme(Request $request, $numero){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $requerimientos = $apiUtils->getRequerimientosByNumeroInforme($numero);

        return $this->render('lista_historial_informe.html.twig', array("requerimientos"=>$requerimientos));
    }

    /**
     * @Route("/listar/requerimiento/actual/bloque", name="_listar_requerimiento_actual_bloque")
     */
    public function listarRequerimientoActualBloque(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $user = $this->get('session')->get('user');
        //$requerimientos = $apiUtils->getRequerimientosByUserInformeActivo($user);
        $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($user["extra"]["bloque"]["id"]);
        $cantPreguntas = 0;
        foreach ($requerimientos as $r) {
            $cantPreguntas = $cantPreguntas + sizeof($r["preguntas"]);
        }

        $diputadosActivos = $this->getDiputadosByRole($user["role"], $request);
        $bloquesActivos = $this->getBloquesByRole($user["role"],$request);
        $interbloques = $apiUtils->getInterbloques();


        return $this->render('listarRequerimientoBloque.html.twig',array("requerimientos"=>$requerimientos,
                                                                                "diputados"=>$diputadosActivos,
                                                                                "bloques"=>$bloquesActivos,
                                                                                "interbloques"=>$interbloques,
                                                                                "cantPreguntas"=>$cantPreguntas));
    }

    /**
     * @Route("/listar/requerimiento/actual/interbloque", name="_listar_requerimiento_actual_interbloque")
     */
    public function listarRequerimientoActualInterbloque(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $user = $this->get('session')->get('user');
        //$requerimientos = $apiUtils->getRequerimientosByUserInformeActivo($user);
        $requerimientos = $apiUtils->getRequerimientosByInterbloqueInformeActivo($user["extra"]["interbloque"]["id"]);
        $cantPreguntas = 0;
        foreach ($requerimientos as $r) {
            $cantPreguntas = $cantPreguntas + sizeof($r["preguntas"]);
        }

        $diputadosActivos = $this->getDiputadosByRole($user["role"], $request);
        $bloquesActivos = $this->getBloquesByRole($user["role"],$request);
        $interbloques = $apiUtils->getInterbloques();


        return $this->render('listarRequerimientoInterbloque.html.twig',array("requerimientos"=>$requerimientos,
            "diputados"=>$diputadosActivos,
            "bloques"=>$bloquesActivos,
            "interbloques"=>$interbloques,
            "cantPreguntas"=>$cantPreguntas));
    }

    /**
    * @Route("/listar/requerimiento/usuario", name="_listar_requerimiento_usuario")
    */
    /*public function listarRequerimientoUsuario(){

        $apiUtils = new ApiUtil($this->get('session'));
        $user = $this->get('session')->get('user');
        $requerimientos = $apiUtils->getRequerimientosByBloque($user["extra"]["bloque"]["id"]);

        return $this->render('listarRequerimiento.html.twig',array("requerimientos"=>$requerimientos));
    }*/

    /**
    * @Route("/informacion", name="_informacion_general")
    */
    public function informacionGeneral(){
        return $this->render('informacionGeneral.html.twig');
    }

    /**
    * @Route("/admin", name="_admin_panel")
    */
    public function panelAdmin(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $user = $this->get('session')->get('user');

        if($_ENV['JGM_DEBUG']){
            $rolByCuil = $apiUtils->getRolesByCuil();
        }

        $users = $apiUtils->getJGMUsers();
        $apiUtils->updateExtras();
        $extras = $apiUtils->getExtras();
        $user = $this->get('session')->get('user');

        $diputadosActivos = $apiUtils->getDiputados();//$this->getDiputadosByRole($user["role"]);
        $bloquesActivos = $apiUtils->getBloquesActivos();//$this->getBloquesByRole($user["role"]);
        $interbloques = $apiUtils->getInterbloques();//$this->getInterbloquesByRole($user["role"]);
        

        $arrUsers = [];
        $i=0;

        foreach ($extras as $extra){

            $arrUsers[$i]["id"] = $extra["id"];
            $userData = $this->getDataByCuil($extra["cuil"],$users); 
            
            $arrUsers[$i]["nombre"] = $extra["nombre"];
            $arrUsers[$i]["apellido"] = $extra["apellido"];
            $arrUsers[$i]["cuil"] = $extra["cuil"];
            $arrUsers[$i]["rol"] = $extra["rol"];

            $arrUsers[$i]["asociacion"] = $this->getAsociacion($arrUsers[$i]["rol"], $extra);

            $i++;
        }

        $result = null;
        if($this->get('session')->get('result')){
            $result = $this->get('session')->get("result");
            $this->get('session')->set("result", null);
        }

        return $this->render('panel_admin.html.twig', array("extras"=>$arrUsers,
                                                                    "diputados"=>$diputadosActivos,
                                                                    "bloques"=>$bloquesActivos,
                                                                    "interbloques"=>$interbloques,
                                                                    "historial_informes"=>"",
                                                                    "result"=>$result));
    }

    /**
     * @Route("/estado", name="_estado_envio")
     */
    public function estadoEnvio(){

        return $this->render('estadoEnvio.html.twig');
    }

    /**
    * @Route("/normativa", name="_normativa")
    */
    public function normativa(){
        return $this->render('normativa.html.twig');
    }
     
     
    /**
    * @Route("/soporte", name="_soporte")
    */
    public function soporte(){
        return $this->render('soporte.html.twig');
    } 
    
    /**
    * @Route("/logout", name="_log_out")
    */
    public function logout(){
        
        $this->get('session')->remove('hcdn-user');
        $this->get('session')->remove('user');
        
        return new RedirectResponse("https://dse.hcdn.gob.ar/sei/web/gabinete");
    }
    
    /**
    * @Route("/informe", name="_informe")
    */
    public function informe(){
        return $this->render('informe.html.twig');
    }
    
    /**
    * @Route("/bloque/fecha/fin", name="_set_fecha_fin")
    */
    public function setFechaFinInformeDeBloque(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $user = $this->get('session')->get('user');
        $requerimientos = null;

        $envioRequerimiento = $apiUtils->getEnvioRequerimientoByUser($this->get('session')->get('user'));

        return $this->render('fecha_fin_informe_bloque.html.twig',array("envioRequerimiento"=>$envioRequerimiento));
    }

    /**
     * @Route("/interbloque/fecha/fin", name="_set_fecha_fin_interbloque")
     */
    public function setFechaFinInformeDeInterbloque(){
        return $this->render('fecha_fin_informe_interbloque.html.twig');
    }


    /**
     * @Route("/requerimientos/enviar", name="_enviar_requerimientos")
     */
    public function enviarRequerimientos(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $user = $this->get('session')->get('user');
        $requerimientos = null;

        $envioRequerimiento = $apiUtils->getEnvioRequerimientoByUser($this->get('session')->get('user'));

        if($envioRequerimiento){

            $data = $envioRequerimiento["token"];
            $crypted = "asd123";

            //$pk = openssl_pkey_get_private("file:/" . $this->get('kernel')->getProjectDir().'/certs/infojgm');
            //$pk = openssl_pkey_get_private("file:///var/www/dse2.hcdn.gob.ar/public_html/public/jgm4/certs/infojgm");
            $pk = openssl_pkey_get_private(
"-----BEGIN RSA PRIVATE KEY-----
MIIEpgIBAAKCAQEAr8tj4fXX/1b8MHvl4UvGZP1YNRDFFADqXCZDvoifIgVZ4P3O
J8vZ76DEBohoCUnLqAHLAQoBhu2QempwGQRkbFGOTJqZhhAaZXvQZextuY0GveCb
ZFIf+oa4+ED5++u90NBdcEMHTC0xm4dEdxMRijPrgKfE36tA2kg4RvyvhF+X3oiH
xHYxtpqmJw2H6jVMIfItkfiYyhpCDcx7DDEDUHb8vwZv4RwEWYcTQket5QW5q0TE
rrES/nP1AKgSZif3UGQxiH5753pSAb62cYfi8Hj6OYA8U3TPZFh2Wj4Gbn1jDk2z
QsVY+Ne7fX6jP43OdaxGdFwW5h6dCvieNbYu8QIDAQABAoIBAQCr814336jvcb4s
DoTE4sGkdsqY3VZoQpz6ldMdw7UkKefIU3obIyKBaddULIjWzQAPzfEriwYDXMw9
cW9EiS9RZFS0SWRx5b5sFbe6epC5NmCpydE1R79P5gd3jNb4u5ZncehzrpysvmgP
oTgu4M//RtnMdk30GibNXGbvyKpF5zuLWcuUQFqWCo/Ux1R+jaRkZMLyznG6MHmx
JWDToU7QAcdPwVkZzeQijbok2VAjd8id4/b4Dfqbn6AiPCj0+tGxNDEv9kDojTff
reg1MIYio9OWqEQ/QlhTxk0kvIqbE16GGWTsnXsrUSZNzfBvaA0RpceleRkfnmW1
8hxN1VMhAoGBAOC1mGULakB0B93CrKLRjqDd7Z+vco8CKE4TKuMp1x0ItVON1Tfi
IoImL88VuUbr9jm0GpcVPpfCuQd/fu7j5ENtc497tynq/IB+A7jX+5EAzNxJT4uu
sf+gSN3mTCW+tA7JeuUf8C4HmQjIrh/KnEEJvUJZK8UIGQqy+cxcPfADAoGBAMhG
Fdaxtv9CfSmUr/YZ43RWvBk+fx6LdWdmpsLRgYmPzAv4Mbn669fR0/DgBtvJtrtV
lcKGe3K6jh8cy2wXIKEFY3HjN2QCgVS83MrKO+znW6WUc6xtp/oSmEQIsDocYtVV
TxpncpOqPTCmW2YbbE+gkp30h1HtpXNWNP6kE/T7AoGBAKpP+Nlc9YM0bmHeEwq0
2e2DR38ocyOA9xt126OhKrGO0CAr99cLRixu1P2X8X2Xil/h6eVo7LAJnkPYllRp
1bBlXJj7o6/ZsteQqzGGQd19RcgQqIGuPqsIyTGmQ0C4cbBjdQoEUv4QNCU8NP1T
i2qf/xoyi8IiiRha3onNwAQRAoGBALB3ILMF8CrG2SYUP19nvEU8T40mxjeRxicQ
CgxLMgqVy8MYNCidccW+uzLZEvICsKIsbULz4D5dJvE/G/boAfYfm1o0TRAwLOkP
aLS+sUKzMMubHLln/cSZMdVwmmb5sBQtkP/A+vIl5LjDPHGh5DPdVpMbbUjBTzFr
WlKtHHCVAoGBAMrxpB+DQSMH3tvP+4sCnEQAq+WP/kUznhsjQIMsKDXrg8tkAaLu
TU6i6PqAz79Fe3hLxftlEJYogMk47SmAAqcsae8+E1PYdsGC46K0rDPOaU/zHzM6
Lhw7TIPlQkRgQmBCiJdds8xR2X0llDs7Js8p56+ADe7QqowDR+8ih5od
-----END RSA PRIVATE KEY-----"
            );
            //die();
            $result = openssl_private_encrypt($data, $crypted, $pk, OPENSSL_PKCS1_PADDING);
            if($result==false){
                die("Fail");
            }

            $envioRequerimiento["token"] = base64_encode($crypted);
        }

        if($user["role"]==$_ENV['ROL_RESPONSABLE_BLOQUE']){
            $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($user["extra"]["bloque"]["id"]);
        }elseif($user["role"]==$_ENV['ROL_RESPONSABLE_INTERBLOQUE']){
            $requerimientos = $apiUtils->getRequerimientosByInterbloqueInformeActivo(($user["extra"]["interbloque"]["id"]));
        }else{
            die("Rol no autorizado");
        }

        $cantPreguntas = 0;
        foreach ($requerimientos as $r) {
            $cantPreguntas = $cantPreguntas + sizeof($r["preguntas"]);
        }

        return $this->render('enviar_requerimientos.html.twig', array("envioRequerimiento"=>$envioRequerimiento,
                                                                            "requerimientos"=>$requerimientos,
                                                                            "cantPreguntas"=>$cantPreguntas));
    }

    /**
     * @Route("/requerimientos/validar", name="_validar_requerimientos")
     */
    public function validarRequerimientos(Request $request){

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $bloquesActivos = $apiUtils->getBloquesActivosSinInterbloque();//$this->getBloquesByRole($user["role"]);
        $interbloques = $apiUtils->getInterbloques();//$this->getInterbloquesByRole($user["role"]);

        $arrVal = array();
        $i = 0;

        foreach ($bloquesActivos as $b){

            $envioRequerimiento = $apiUtils->getEnvioRequerimientoByBloque($b["bloque"]["id"]);
            $arrRequerimientosBloque = $apiUtils->getRequerimientosByBloqueInformeActivo($b["bloque"]["id"]);

            if($envioRequerimiento){
                //echo $envioRequerimiento->getId();

                $requerimientosBloque = $apiUtils->getRequerimientosByEnvioRequerimiento($envioRequerimiento);

                $arrVal[$i]["id"] = $b["bloque"]["id"];
                $arrVal[$i]["nombre"] = $b["bloque"]["nombre"];
                $arrVal[$i]["cant"] = $b["integrantes"];
                $arrVal[$i]["tipo"] = "bloque";
                $arrVal[$i]["envio_req"] = $envioRequerimiento;
                $arrVal[$i]["cantReq"] = sizeof($envioRequerimiento["requerimientos"]) ;
                $arrVal[$i]["cantPreg"] = 0;
                foreach ($requerimientosBloque as $r){
                    $arrVal[$i]["cantPreg"] = $arrVal[$i]["cantPreg"] + sizeof($r["preguntas"]);
                }

                $i++;
            }elseif(sizeof($arrRequerimientosBloque) > 0 ){

                $arrVal[$i]["id"] = $b["bloque"]["id"];
                $arrVal[$i]["nombre"] = $b["bloque"]["nombre"];
                $arrVal[$i]["cant"] = $b["integrantes"];
                $arrVal[$i]["tipo"] = "bloque";
                $arrVal[$i]["envio_req"] = null;
                $arrVal[$i]["cantReq"] = sizeof($arrRequerimientosBloque) ;
                $arrVal[$i]["cantPreg"] = 0;
                foreach ($arrRequerimientosBloque as $r){
                    $arrVal[$i]["cantPreg"] = $arrVal[$i]["cantPreg"] + sizeof($r["preguntas"]);
                }
                $i++;
            }
        }

        foreach ($interbloques as $interbloque){
            $envioRequerimientoInterbloque = $apiUtils->getEnvioRequerimientoByInterbloque($interbloque["id"]);
            $requerimientosInterbloque = $apiUtils->getRequerimientosByInterbloqueInformeActivo($interbloque["id"]);

            if($envioRequerimientoInterbloque){
                //echo $envioRequerimientoInterbloque->getId();
                //$requerimientosInterbloque = $apiUtils->getRequerimientosByEnvioRequerimiento($envioRequerimientoInterbloque);

                $arrVal[$i]["id"] = $interbloque["id"];
                $arrVal[$i]["nombre"] = $interbloque["nombre"];
                $arrVal[$i]["cant"] = $interbloque["cantidad"];
                $arrVal[$i]["tipo"] = "interbloque";
                $arrVal[$i]["envio_req"] = $envioRequerimientoInterbloque;
                $arrVal[$i]["cantPreg"] = 0;
                foreach ($requerimientosInterbloque as $r){
                    $arrVal[$i]["cantPreg"] = $arrVal[$i]["cantPreg"] + sizeof($r["preguntas"]);
                }

                $i++;
            }elseif(sizeof($requerimientosInterbloque) > 0 ){

                $arrVal[$i]["id"] = $interbloque["id"];
                $arrVal[$i]["nombre"] = $interbloque["nombre"];
                $arrVal[$i]["cant"] = $interbloque["cantidad"];
                $arrVal[$i]["tipo"] = "interbloque";
                $arrVal[$i]["envio_req"] = null;
                $arrVal[$i]["cantReq"] = sizeof($requerimientosInterbloque) ;
                $arrVal[$i]["cantPreg"] = 0;
                foreach ($requerimientosInterbloque as $r){
                    $arrVal[$i]["cantPreg"] = $arrVal[$i]["cantPreg"] + sizeof($r["preguntas"]);
                }
                $i++;
            }

        }

        usort ( $arrVal, function($a, $b){
            if ($a["cant"] == $b["cant"]) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        } );

        return $this->render('validarRequerimientos.html.twig', array("validacion"=>$arrVal));
    }

     /**
    * @Route("/migrar", name="_migrar")
    */
    public function migrar(){

        $this->migrarProvincias();
        $this->migrarBloques();
        //$this->migrarOrganos();
        $this->migrarInterbloques();
        $this->migrarDiputados();
        //$this->crearExtras();

        return new Response('Migración exitosa');
    }

    private function crearExtras(){

        $dm = $this->get('doctrine_mongodb')->getManager();
        /*$em = $this->getDoctrine()->getManager();*/

        //MIGRO BLOQUES
//        $users = $em->getRepository('STIRBundle:Bloque')->findAll();
  //      $usersMongo = RolestackUtil::getUsers();

        /*foreach ($users as $user){

            //$bloque = $user->getBloque();

        }*/

        $extras = $dm->getRepository('App:Extra')->findAll();


        $dm = $this->get('doctrine_mongodb')->getManager();
        $em = $this->getDoctrine()->getManager();

        //MIGRO BLOQUES
        $bloques = $em->getRepository('STIRBundle:Bloque')->findAll();

        foreach($bloques as $bloque){
            if($bloque->getId()>0){

                $bloqueMongo = $dm->getRepository('App:Bloque')->findOneByNombre($bloque->getNombre());
                if(!$bloqueMongo){
                    $bloqueMongo = new Bloque();
                }

                $bloqueMongo ->setNombre($bloque->getNombre());

                $dm->persist($bloqueMongo);

            }
        }

        $dm->flush();

    }

    private function migrarBloques(){
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        $em = $this->getDoctrine()->getManager();
        
        $bloques = $em->getRepository('STIRBundle:Bloque')->findAll();
        
        foreach($bloques as $bloque){
            if($bloque->getId()>0){
                
                $bloqueMongo = $dm->getRepository('App:Bloque')->findOneByNombre($bloque->getNombre());
                if(!$bloqueMongo){
                    $bloqueMongo = new Bloque();
                }
                
                $bloqueMongo ->setNombre($bloque->getNombre());
                
                $dm->persist($bloqueMongo);
            }
        }
        
        $dm->flush();
        
    }

    private function migrarOrganos(){

        $dm = $this->get('doctrine_mongodb')->getManager();
        $em = $this->getDoctrine()->getManager();

        $organos = $em->getRepository('STIRBundle:Organo')->findAll();

        foreach($organos as $organo){

                $organoMongo = $dm->getRepository('App:Organo')->findOneByNombre($organo->getNombre());
                if(!$organoMongo){
                    $organoMongo = new Organo();
                }

                $organoMongo->setNombre($organo->getNombre());

                $dm->persist($organoMongo);

        }

        $dm->flush();

    }

    private function migrarInterbloques(){

        $dm = $this->get('doctrine_mongodb')->getManager();
        $em = $this->getDoctrine()->getManager();

        $interbloques = $em->getRepository('STIRBundle:Interbloque')->findAll();

        foreach($interbloques as $interbloque){

                $interBloqueMongo = $dm->getRepository('App:Interbloque')->findOneByNombre($interbloque->getNombre());

                if(!$interBloqueMongo){
                    $interBloqueMongo = new Interbloque();
                }

                $interBloqueMongo ->setNombre($interbloque->getNombre());

                $bloquesInterbloque  = $em->getRepository('STIRBundle:Bloque')->findBy(array("interbloque"=>$interbloque));

                foreach ($bloquesInterbloque as $bloque){
                    $bloqueMongo = $dm->getRepository('App:Bloque')->findOneByNombre($bloque->getNombre());
                    if($bloqueMongo){
                        $interBloqueMongo->addBloque($bloqueMongo);
                    }

                }

                $dm->persist($interBloqueMongo);

        }

        $dm->flush();

    }
    
    private function migrarDiputados(){
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        $em = $this->getDoctrine()->getManager();
        
        //MIGRO DIPUTADOS
        $diputados = $em->getRepository('STIRBundle:Diputado')->getDiputadosActivos();
        
        foreach($diputados as $diputado){
                
            $diputadoMongo = $dm->getRepository('App:Diputado')->findOneByCuil($diputado->getCuil());
            if(!$diputadoMongo){
                $diputadoMongo = new Diputado();
            }

            $diputadoMongo->setNombre($diputado->getNombre());
            $diputadoMongo->setApellido($diputado->getApellido());
            $diputadoMongo->setCuil($diputado->getCuil());
            $diputadoMongo ->setBloque($dm->getRepository('App:Bloque')->findOneByNombre($diputado->getBloque()->getNombre()));
            $diputadoMongo ->setProvincia($dm->getRepository('App:Provincia')->findOneByNombre($diputado->getProvincia()->getNombre()));
            
            $dm->persist($diputadoMongo);

        }
        
        $dm->flush();
    }
    
    private function migrarProvincias(){
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        $em = $this->getDoctrine()->getManager();
        
        //MIGRO DIPUTADOS
        $provincias = $em->getRepository('STIRBundle:Provincia')->findAll();
        
        foreach($provincias as $provincia){
                
            if($provincia->getId()!=999){
                
                $provinciaMongo = $dm->getRepository('App:Provincia')->findOneByNombre($provincia->getNombre());
                if(!$provinciaMongo){
                    $provinciaMongo = new Provincia();
                }
                
                
                $provinciaMongo->setNombre($provincia->getNombre());
                
                $dm->persist($provinciaMongo);
                
            }
            
        }
        
        $dm->flush();
     
    }

    /**
     * @Route("/ajax/update-user", name="ajax-update-user")
     */
    public function ajaxUpdateUser(Request $request)
    {

        try{

            $cuilUser = $request->get("user-cuil");
            $bloqueId = $request->get("bloque-id");
            $interbloqueId = $request->get("interbloque-id");
            $diputadoId = $request->get("diputado-id");

            $dm = $this->get('doctrine_mongodb')->getManager();

            $extra = $dm->getRepository('App:Extra')->findOneByCuil($cuilUser);
            $extra = $dm->find('App:Extra',$extra->getId());

            $bloque = $dm->getRepository('App:Bloque')->findOneById($bloqueId);
            $interbloque = $dm->getRepository('App:Interbloque')->findOneById($interbloqueId);
            $diputado = $dm->getRepository('App:Diputado')->findOneById($diputadoId);

            $extra->setBloque($bloqueId == -1 ? null : $bloque );
            $extra->setInterbloque($interbloqueId == -1 ? null : $interbloque);
            $extra->setDiputado($diputadoId == -1 ? null : $diputado);

            $dm->persist($extra);
            $dm->flush();

            $success = true;
        }catch (\Exception $e){
            return new JsonResponse(array("success"=>false,"error"=>$e->getMessage()));
        }

        return new JsonResponse(array("success"=>$success,"id"=>$extra->getId()));
    }

    private function getDataByCuil($cuil, $arrRoles){

        foreach ($arrRoles as $a) {
            if ($a["cuil"] == $cuil){
                return $a;
            }
        }

        return null;
    }

    private function getAsociacion($rol, $extra)
    {

        $asociacion = null;

        switch ($rol) {
            case $_ENV['ROL_DIPUTADO']:
                //no hago nada
                break;
            case $_ENV['ROL_ASESOR']:
                if (isset($extra["diputado"])){
                    $asociacion = $extra["diputado"]["apellido"].", ".$extra["diputado"]["nombre"];   
                }
                break;
            case $_ENV['ROL_RESPONSABLE_BLOQUE']:
                
                if (isset($extra["bloque"])){
                    $asociacion = $extra["bloque"]["nombre"];
                }
                break;
            case $_ENV['ROL_RESPONSABLE_INTERBLOQUE']:
                if (isset($extra["interbloque"])){
                    $asociacion = $extra["interbloque"]["nombre"];   
                }
                break;
            case $_ENV['ROL_PERSONAL_PARLAMENTARIO']:
                if (isset($extra["bloque"])){
                    $asociacion = $extra["bloque"]["nombre"];   
                }
                break;
            case $_ENV['ROL_ADMIN']:
                break;
            case $_ENV['ROL_DIRECCION_SECRETARIA']:
                //no hago nada
                break;
        }

        return $asociacion;

    }
    
    private function getDiputadosByRole($role, $request){
        
        $diputados = null;
        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        switch ($role) {
            case $_ENV['ROL_DIPUTADO']:
                $diputados = $apiUtils->getDiputadoByCuil($this->get('session')->get('user')["cuil"]);
                break;
            case $_ENV['ROL_ASESOR']:
                $diputados = $apiUtils->getDiputadoByAsesor($this->get('session')->get('user')["extra"]["diputado"]["cuil"]);
                break;
            case $_ENV['ROL_PERSONAL_PARLAMENTARIO']:
                $diputados = $apiUtils->getDiputadosByBloque($this->get('session')->get('user')["extra"]["bloque"]);
                break;
            case $_ENV['ROL_RESPONSABLE_BLOQUE']:
                $diputados = $apiUtils->getDiputadosByBloque($this->get('session')->get('user')["extra"]["bloque"]);
                break;
            case $_ENV['ROL_RESPONSABLE_INTERBLOQUE']:
                $diputados = $apiUtils->getDiputados($this->get('session')->get('user')["extra"]["interbloque"]);
                break;
            case $_ENV['ROL_ADMIN']:
                //a
                break;
            case $_ENV['ROL_DIRECCION_SECRETARIA']:
                //no hago nada
                break;
        }   
        
        return $diputados;
    }
    
    private function getRequerimientosByRole($role, $request){
        
        $requerimientos = null;
        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        
        switch ($role) {
            case $_ENV['ROL_DIPUTADO']:
                $requerimientos = $apiUtils->getRequerimientosByDiputadoInformeActivo($this->get('session')->get('user')["cuil"]);
                break;
            case $_ENV['ROL_ASESOR']:
                $requerimientos = $apiUtils->getRequerimientosByDiputadoInformeActivo($this->get('session')->get('user')["extra"]["diputado"]["cuil"]);
                break;
            case $_ENV['ROL_PERSONAL_PARLAMENTARIO']:
                $requerimientos = $apiUtils->getRequerimientosByUser($this->get('session')->get('user'));
                break;
            case $_ENV['ROL_RESPONSABLE_BLOQUE']:
                $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($this->get('session')->get('user')["extra"]["bloque"]["id"]);
                //print_r($requerimientos);
                //die("");
                break;
            case $_ENV['ROL_RESPONSABLE_INTERBLOQUE']:
                $requerimientos = null;
            case $_ENV['ROL_ADMIN']:
                $requerimientos = null;
                break;
            case $_ENV['ROL_DIRECCION_SECRETARIA']:
                //no hago nada
                break;
        }
        
        return $requerimientos;
        
    }
    
    private function getBloquesByRole($role, $request){
        
        $bloques = null;
        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        
        switch ($role) {
            case $_ENV['ROL_DIPUTADO']:
                $bloques = null;#$apiUtils->getBloqueByCuil($this->get('session')->get('user')["cuil"]);
                break;
            case $_ENV['ROL_ASESOR']:
                $bloques = null;#$apiUtils->getDiputadoByCuil($this->get('session')->get('user')["cuil"]);
                break;
            case $_ENV['ROL_PERSONAL_PARLAMENTARIO']:
                $bloques = $apiUtils->getBloqueById($this->get('session')->get('user')["extra"]["bloque"]["id"]);
                $bloques = [$bloques];
                break;
            case $_ENV['ROL_RESPONSABLE_BLOQUE']:
                $bloques = [$apiUtils->getBloqueById($this->get('session')->get('user')["extra"]["bloque"]["id"])];
                break;
            case $_ENV['ROL_RESPONSABLE_INTERBLOQUE']:
                $bloques = $apiUtils->getBloquesActivos();
                break;
            case $_ENV['ROL_ADMIN']:
                $bloques = null;
                break;
            case $_ENV['ROL_DIRECCION_SECRETARIA']:
                //no hago nada
                break;
        }
        
        return $bloques;
    }

}
