<?php

namespace App\Controller;

use App\Entity\Log;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Utils\ApiUtil;
use Endroid\QrCode\QrCode;
use \Datetime;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class PostController extends Controller
{

    private $apiUtil = null;
    
    /**
     * @Route("/save-informe", name="_save_informe")
     */
    public function saveInforme(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $numeroInforme = $request->get('informe');
        $fechaInicio = $request->get('date-start');
        $fechaFin = $request->get('date-end');
        $fechaVisita = $request->get('date-visita');
        $horaInicio = $request->get('hour-start');
        $horaFin = $request->get('hour-end');
        $expediente = $request->get('expediente');
         $tipo = $request->get('type');
        $nota = $request->get('nota');
        $notaArchivo = $request->get('nota-file');
        
        $dateInicio = str_replace('/','-',$fechaInicio)." ".$horaInicio;
        $dateFin =  str_replace('/','-',$fechaFin)." ".$horaFin;
        //$fechaFin = DateTime::createFromFormat('d/M/Y H:i', $dateFin);
        $dateVisita = null;
        if($dateVisita){
            $dateVisita =  str_replace('/','-',$fechaVisita)." "."00:00";
        }

        //die($dateVisita);

        $req = array("numero"=>(int)$numeroInforme,
                        "fecha_inicio"=>$dateInicio,
                        "fecha_fin"=>$dateFin,
                        "fecha_visita"=>$dateVisita,
                        "expediente"=>$expediente
                        //"nota"=>$nota,
                        //"nota_archivo"=>$notaArchivo,
                        );

        $result = $this->apiUtil->saveInforme($req);
        $this->get('session')->set("result", $result);

        return $this->redirectToRoute("_informe",array("result"=>$result));
    }

    /**
     * @Route("/delete-informe", name="_delete_informe")
     */
    public function deleteInforme(Request $request)
    {

        return $this->redirectToRoute("_informe");

        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $idInforme = $request->get('id-informe-cancel');

        die($idInforme."d");

        $result = $this->apiUtil->deleteInforme($idInforme);

        $this->get('session')->set("result", $result);

        return $this->redirectToRoute("_informe");
    }




    /**
     * @Route("/add-pregunta-informe", name="add_pregunta_by_req_id")
     */
    public function addPreguntaByInformeId(Request $request)
    {

        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $idReq = $request->get('idReq');
        $texto = $request->get('texto');


        $req = array("id-req"=>$idReq,
                      "texto"=>$texto);

        $result = $this->apiUtil->addPreguntaInRequerimiento($req);

        $this->get('session')->set("result", $result);

        return $this->redirectToRoute("_informe");
    }

    /**
     * @Route("/save-fecha-informe-interbloque", name="_save_fecha_fin_interbloque")
     */
    public function setFechaFinInformeInterbloque(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $bloqueId = $request->get('interbloque-id');
        $fechaFin = $request->get('date-end');
        $horaFin = $request->get('hr-end');
        //die($horaFin);
        $user = $this->get('session')->get('user');

        $dateFin =  str_replace('/','-',$fechaFin)." ".$horaFin;

        $req = array("interbloque_id"=>$bloqueId,
            "fecha_fin"=>$dateFin);

        $this->apiUtil->setFechaFinInformeInterbloque($req);

        $this->get('session')->set("result", "Fecha modificada correctamente");

        return $this->redirect($this->generateUrl('_set_fecha_fin'));
    }


    /**
     * @Route("/save-fecha-informe-bloque", name="_save_fecha_fin_bloque")
     */
    public function setFechaFinInformeBloque(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $bloqueId = $request->get('bloque-id');
        $fechaFin = $request->get('date-end');
        $horaFin = $request->get('hr-end');
        //die($horaFin);
        $user = $this->get('session')->get('user');
      
        $dateFin =  str_replace('/','-',$fechaFin)." ".$horaFin;

        //die($dateFin);
        
        $req = array("bloque_id"=>$bloqueId,
                        "fecha_fin"=>$dateFin);


        $this->apiUtil->setFechaFinInformeBloque($req);

        $this->get('session')->set("result", "Fecha modificada correctamente");

        return $this->redirect($this->generateUrl('_set_fecha_fin'));
    }



    /**
     * @Route("/enviar-requerimientos-post", name="_enviar_req_post")
     */
    public function enviarRequerimientos(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $id = $request->get('id');
        $token = new QrCode($this->getToken());
        $req = null;

        if($this->get('session')->get('user')["role"]==$_ENV["ROL_RESPONSABLE_BLOQUE"]){
            $req = array("idBloque"=>$id,
                "idInterbloque"=>null,
                "token"=>$token->getText());
        }else{
            $req = array("idBloque"=>null,
                "idInterbloque"=>$id,
                "idBloque"=>null,
                "token"=>$token->getText());
        }

        $this->apiUtil->enviarRequerimientos($req);

        return $this->redirect($this->generateUrl('_enviar_requerimientos'));
    }



    /**
     * @Route("/validar-requerimientos-post", name="_validar_requerimientos_post")
     */
    public function validarRequerimientos(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $token = $request->get('qr_code');
        $tipoValidacion = $request->get('tipo');

        $req = array("token"=>$token);

        $respuesta = $this->apiUtil->validarRequerimientos($req, $tipoValidacion);

        return new JsonResponse($respuesta);
    }



    /**
     * @Route("/add-expediente-to-envio-req", name="_add_expediente_to_envio_req")
     */
    public function addExpedienteToEnvioRequerimiento(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $textExpediente = $request->get('expediente_text');
        $tipoValidacion = $request->get('tipo');
        $idEnvioReq = $request->get('id_envio_requerimiento');

        $req = array("expediente"=>$textExpediente,
                    "id_envio_req"=>$idEnvioReq);

        $respuesta = $this->apiUtil->agregarExpediente($req, $tipoValidacion);

        return new JsonResponse($respuesta);
    }



    /**
     * @Route("/exportar_requerimientos", name="_exportar_requerimientos")
     */
    public function exportarReqeuerimiento(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $respuesta = $this->apiUtil->exportarRequerimientos();

        return new JsonResponse($respuesta);
    }


    /**
     * @Route("/json_exportado", name="_json_exportado_last")
     */
    public function getJsonExportado(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $respuesta = $this->apiUtil->getJsonExportado();

        $token = md5 ( rand () );
        $filename = $token . '.json';
        $current = $this->get ( 'kernel' )->getRootDir () . '/../temp/' . $filename;

        try {
            file_put_contents($current, $respuesta);
        }
        catch(IOException $e) {
        }

        $httpDir = $this->getHttpDir($request,$filename);

        return new JsonResponse($httpDir);
    }

    /**
     * @Route("/excel_exportado", name="_excel_exportado_last")
     */
    public function getExcelExportado(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $respuesta = $this->apiUtil->getArryPHPJsonExportado();

        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        //$sheet = $this->createCabeceraExcel($sheet);
        $rows = 1;

        foreach ($respuesta["interbloques"] as $requerimientosInterbloque) {
            $resp = $this->addRequerimientoInterbloqueExcel($sheet, $requerimientosInterbloque, $rows);
            $sheet = $resp[0];
            $rows = $resp[1];
        }

        foreach ($respuesta["bloques"] as $bloque) {
            $this->addRequerimientoBloqueExcel($sheet, $bloque, $rows);
        }

        $temp_file = null;
        $current = $this->get ( 'kernel' )->getRootDir () . '/../temp';

        try {
            // Create your Office 2007 Excel (XLSX Format)
            $writer = new Xlsx($spreadsheet);

            // Create a Temporary file in the system
            $temp_file = tempnam($current, "excel_");
            rename($temp_file, $temp_file .= '.xlsx');
            $filename = explode('/',$temp_file);
            $filename = $filename[sizeof($filename)-1];

            // Create the excel file in the tmp directory of the system
            $writer->save($temp_file);
            //return new JsonResponse($temp_file);

        }
        catch(\Exception $e) {
            die($e->getMessage());
        }

        $httpDir = $this->getHttpDir($request, $filename);

        return new JsonResponse($httpDir);
    }

    /**
     * @Route("/check-todo-validado", name="_check_todo_validado")
     */
    public function checkTodoValidado(Request $request)
    {

        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        //return new JsonResponse("JUA JUA");

        $bloquesActivos = $apiUtils->getBloquesActivosSinInterbloque();//$this->getBloquesByRole($user["role"]);
        $interbloques = $apiUtils->getInterbloques();//$this->getInterbloquesByRole($user["role"]);

        $arrVal = array();
        $i = 0;
        $sinValidar = array();

        foreach ($bloquesActivos as $b){
            $envioRequerimiento = $apiUtils->getEnvioRequerimientoByBloque($b["bloque"]["id"]);

            //return new JsonResponse($envioRequerimiento["requerimientos"]);
            //die("");
            if($envioRequerimiento != null  && $envioRequerimiento["validado"] == false && sizeof($envioRequerimiento["requerimientos"]) > 0){
                $sinValidar[$i]["id"] = $b["bloque"]["id"];
                $sinValidar[$i]["nombre"] = $b["bloque"]["nombre"];
                $sinValidar[$i]["tipo"] = 'bloque';
            }

            $i++;
        }

        foreach ($interbloques as $interbloque){
            $envioRequerimientoInterbloque = $apiUtils->getEnvioRequerimientoByInterbloque($interbloque["id"]);
            $requerimientosInterbloque = $apiUtils->getRequerimientosByInterbloqueInformeActivo($interbloque["id"]);

            if($envioRequerimientoInterbloque && $envioRequerimientoInterbloque["validado"] == false && sizeof($requerimientosInterbloque)>0){

                $sinValidar[$i]["id"] = $interbloque["id"];
                $sinValidar[$i]["nombre"] = $interbloque["nombre"];
                $sinValidar[$i]["tipo"] = 'interbloque';

            }

            $i++;
        }

        usort ( $sinValidar, function($a, $b){
            if ($a["tipo"] == $b["tipo"]) {
                return 0;
            }
            return ($a["tipo"] =='interbloque') ? -1 : 1;
        } );

        return new JsonResponse(array("sinValidar"=>$sinValidar));

    }

    /**
     * @Route("/save-requerimiento", name="saveRequerimiento")
     */
    public function saveRequerimiento(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $introduccion = $request->get('introduccion');
        $bloque_id = $request->get('bloque');
        $diputado_id = $request->get('diputado');
        $preguntas = explode('[&][%]',$request->get('preguntas'));
        $user = $this->get('session')->get('user');

        $req = array("introduccion"=>$introduccion,
                        "preguntas"=>$preguntas,
                        "bloque_id"=>$bloque_id,
                        "diputado_id"=>$diputado_id,
                        "user"=>$user);

        $result = $this->apiUtil->saveRequerimiento($req);
        $this->get('session')->set("result", $result);

        return $this->redirectToRoute("_crear_requerimiento");
    }

    /**
     * @Route("/edit-extra", name="edit_extra")
     */
    public function editExtra(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $bloque_id = $request->get('bloque_id');
        $interbloque_id = $request->get('interbloque_id');
        $diputado_id = $request->get('diputado_id');
        $extra_id = $request->get('extra_id');

        $req = array(
            "bloque_id"=>$bloque_id,
            "diputado_id"=>$diputado_id,
            "extra_id"=>$extra_id,
            "interbloque_id"=>$interbloque_id
        );

        $result = $this->apiUtil->editExtra($req);
        $this->get('session')->set("result", $result);

        return $this->redirectToRoute("_admin_panel");
    }
    
    /**
     * @Route("/get-preguntas-by-requerimiento", name="get_preguntas_by_requerimiento")
     */
    public function getPreguntasByRequerimiento(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
        $requerimiento_id = $request->get('id');

        $preguntas = $this->apiUtil->getPreguntasByRequerimiento($requerimiento_id);

        return new JsonResponse($preguntas);


    }

    /**
     * @Route("/get-requerimiento-by-id", name="get_requerimiento_by_id")
     */
    public function getRequerimientoById(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $requerimiento_id = $request->get('idRequerimiento');

        $requerimiento = $this->apiUtil->getRequerimientosById($requerimiento_id);

        return new JsonResponse($requerimiento);

    }

    /**
     * @Route("/delete-requerimiento-by-id", name="delete_requerimiento_by_id")
     */
    public function deleteRequerimientoById(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $requerimiento_id = $request->get('idRequerimiento');

        $result = $this->apiUtil->deleteRequerimientosById($requerimiento_id);

        return new JsonResponse($result);

    }

    /**
     * @Route("/update-requerimiento-by-id", name="update_requerimiento_by_id")
     */
    public function updateRequerimientoById(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $requerimiento_id = $request->get('idRequerimiento');
        $introduccion = $request->get('introduccion');
        $idDiputado = $request->get('idDiputado');
        $idBloque = $request->get('idBloque');

        $result = $this->apiUtil->updateRequerimientoById($requerimiento_id, $introduccion, $idDiputado, $idBloque);

        return new JsonResponse($result);

    }

    /**
     * @Route("/get-pregunta-by-id", name="get_pregunta_by_id")
     */
    public function getPreguntaById(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $pregunta_id = $request->get('idPregunta');

        $pregunta = $this->apiUtil->getPreguntaById($pregunta_id);

        return new JsonResponse($pregunta);

    }

    /**
     * @Route("/delete-pregunta-by-id", name="delete_pregunta_by_id")
     */
    public function deletePreguntaById(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $pregunta_id = $request->get('idPregunta');

        $result = $this->apiUtil->deletePreguntaById($pregunta_id);

        return new JsonResponse($result);

    }

    /**
     * @Route("/update-pregunta-by-id", name="update_pregunta_by_id")
     */
    public function updatePreguntaById(Request $request)
    {
        $this->apiUtil = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        $pregunta_id = $request->get('idPregunta');
        $texto = $request->get('texto');

        $result = $this->apiUtil->updatePreguntaById($pregunta_id,$texto);

        return new JsonResponse($result);

    }

    function getToken( $type = 'alnum', $length = 8 )
    {
        switch ( $type ) {
            case 'alnum':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'alpha':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'hexdec':
                $pool = '0123456789abcdef';
                break;
            case 'numeric':
                $pool = '0123456789';
                break;
            case 'nozero':
                $pool = '123456789';
                break;
            case 'distinct':
                $pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
                break;
            default:
                $pool = (string) $type;
                break;
        }


        $crypto_rand_secure = function ( $min, $max ) {
            $range = $max - $min;
            if ( $range < 0 ) return $min; // not so random...
            $log    = log( $range, 2 );
            $bytes  = (int) ( $log / 8 ) + 1; // length in bytes
            $bits   = (int) $log + 1; // length in bits
            $filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
            do {
                $rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ( $rnd >= $range );
            return $min + $rnd;
        };

        $token = "";
        $max   = strlen( $pool );
        for ( $i = 0; $i < $length; $i++ ) {
            $token .= $pool[$crypto_rand_secure( 0, $max )];
        }
        return $token;
    }

    private function getHttpDir(Request $request, $filename){
        return $request->getSchemeAndHttpHost().$request->getBasePath()."/../temp/".$filename;
    }

    private function createCabeceraExcel($sheet){


    }

    private function addRequerimientoInterbloqueExcel($sheet, $requerimientos, $rows){

        $nroFila = $rows;

        $sheet->setCellValue('A'.$nroFila, 'ID');
        $sheet->setCellValue('B'.$nroFila, 'DIPUTADO');
        $sheet->setCellValue('C'.$nroFila, 'INTERBLOQUE');
        $sheet->setCellValue('D'.$nroFila, 'BLOQUE');
        $sheet->setCellValue('E'.$nroFila, 'INTRODUCCION');
        $sheet->setCellValue('F'.$nroFila, 'REQUERIMIENTO');
        $sheet->setCellValue('G'.$nroFila, 'ORGANO REQUERIDO');

        $nroFila++;

        foreach ($requerimientos as $requerimiento){

            if($requerimiento["diputado"]["apellido"] == ""){
                $diputado = "";
            }else{
                $diputado = $requerimiento["diputado"]["apellido"].", ".$requerimiento["diputado"]["nombre"];
            }

            $interbloque = $requerimiento["interbloque"]["nombre"];
            $bloque = $requerimiento["bloque"]["nombre"];
            $introduccion = $requerimiento["introduccion"];

            foreach ($requerimiento["preguntas"] as $pregunta){
                $sheet->setCellValue('A'.$nroFila, $pregunta["id"]);
                $sheet->setCellValue('B'.$nroFila, $diputado);
                $sheet->setCellValue('C'.$nroFila, $interbloque);
                $sheet->setCellValue('D'.$nroFila, $bloque);
                $sheet->setCellValue('E'.$nroFila, $introduccion);
                $sheet->setCellValue('F'.$nroFila, $pregunta["texto"]);
                $sheet->setCellValue('G'.$nroFila, "");
                $nroFila++;
            }

        }

        return array($sheet,$nroFila);
    }

    private function addRequerimientoBloqueExcel($sheet, $requerimientos, $rows){

        $nroFila = $rows;

        foreach ($requerimientos as $requerimiento){

            if($requerimiento["diputado"]["apellido"] == ""){
                $diputado = "";
            }else{
                $diputado = $requerimiento["diputado"]["apellido"].", ".$requerimiento["diputado"]["nombre"];
            }

            $interbloque = $requerimiento["interbloque"]["nombre"];
            $bloque = $requerimiento["bloque"]["nombre"];
            $introduccion = $requerimiento["introduccion"];

            foreach ($requerimiento["preguntas"] as $pregunta){
                $sheet->setCellValue('A'.$nroFila, '###');
                $sheet->setCellValue('B'.$nroFila, $diputado);
                $sheet->setCellValue('C'.$nroFila, $interbloque);
                $sheet->setCellValue('D'.$nroFila, $bloque);
                $sheet->setCellValue('E'.$nroFila, $introduccion);
                $sheet->setCellValue('F'.$nroFila, $pregunta["texto"]);
                $sheet->setCellValue('G'.$nroFila, "");
                $nroFila++;
            }

        }

        return $sheet;
    }
}
