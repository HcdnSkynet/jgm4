<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Utils\ApiUtil;
use Knp\Snappy\Pdf;

class PrintController extends Controller
{

    /**
     * @Route("/pdf/requerimientos", name="pdf_requerimientos")
     */
    public function requerimientosAction(Request $request)
    {
        try{
            $rdir = $this->get('kernel')->getRootDir() . "/../vendor/bin/wkhtmltopdf-amd64";
            $user = $this->get('session')->get('user');

            $snappy = new Pdf($rdir,array("encoding" => "utf-8",
                'footer-right'=>utf8_decode('Página [page] de [topage] - '.date('\ d/m/Y\ H:i')),
                'footer-left'=>utf8_decode('Esto es un borrador.'),
                'header-html'=>utf8_decode($this->renderView('pdf_header.html.twig',array("user"=>$user))),
                'header-spacing' => 35,
                'margin-top' => 50,
            ));

            $html = $this->renderView('req_borrador_impresion.html.twig',array("requerimientos"=>$this->getRequerimientosByRole($user["role"], $request),
                                                                                     "user"=>$user));
        }catch(\Exception $e){
            die("TRY HARD: " . $e->getMessage());
        }
        $d = new \DateTime ();
        return new Response ( $this->makePdf ( $html, $snappy ), 200, array (
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="requerimientos_' . $d->format ( 'd/m/Y' ) . '.pdf"'
            )
        );
    }

    /**
     * @Route("/pdf/requerimientos/bloque", name="pdf_requerimientos_bloque")
     */
    public function requerimientosBloqueAction(Request $request)
    {
        try{
            $requerimientos = null;
            $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

            $rdir = $this->get('kernel')->getRootDir() . "/../vendor/bin/wkhtmltopdf-amd64";
            $user = $this->get('session')->get('user');
            $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($this->get('session')->get('user')["extra"]["bloque"]["id"]);
            $user["role"] = $_ENV['ROL_RESPONSABLE_BLOQUE'];
            $snappy = new Pdf($rdir,array("encoding" => "utf-8",
                'footer-right'=>utf8_decode('Página [page] de [topage] - '.date('\ d/m/Y\ H:i')),
                'footer-left'=>utf8_decode('Esto es un borrador.'),
                'header-html'=>utf8_decode($this->renderView('pdf_header.html.twig',array("user"=>$user))),
                'header-spacing' => 35,
                'margin-top' => 50,
            ));

            $html = $this->renderView('req_borrador_impresion.html.twig',array("requerimientos"=>$requerimientos,
                "user"=>$user));
        }catch(\Exception $e){
            die("TRY HARD: " . $e->getMessage());
        }
        $d = new \DateTime ();
        return new Response ( $this->makePdf ( $html, $snappy ), 200, array (
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="requerimientos_' . $d->format ( 'd/m/Y' ) . '.pdf"'
            )
        );
    }

    /**
     * @Route("/pdf/requerimientos/interbloque", name="pdf_requerimientos_interbloque")
     */
    public function requerimientosInterbloqueAction(Request $request)
    {
        try{
            $requerimientos = null;
            $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

            $rdir = $this->get('kernel')->getRootDir() . "/../vendor/bin/wkhtmltopdf-amd64";
            $user = $this->get('session')->get('user');
            $requerimientos = $apiUtils->getRequerimientosByInterbloqueInformeActivo($this->get('session')->get('user')["extra"]["interbloque"]["id"]);
            $user["role"] = $_ENV['ROL_RESPONSABLE_INTERBLOQUE'];
            $snappy = new Pdf($rdir,array("encoding" => "utf-8",
                'footer-right'=>utf8_decode('Página [page] de [topage] - '.date('\ d/m/Y\ H:i')),
                'footer-left'=>utf8_decode('Esto es un borrador.'),
                'header-html'=>utf8_decode($this->renderView('pdf_header.html.twig',array("user"=>$user))),
                'header-spacing' => 35,
                'margin-top' => 50,
            ));

            $html = $this->renderView('req_borrador_impresion.html.twig',array("requerimientos"=>$requerimientos,
                "user"=>$user));
        }catch(\Exception $e){
            die("TRY HARD: " . $e->getMessage());
        }
        $d = new \DateTime ();
        return new Response ( $this->makePdf ( $html, $snappy ), 200, array (
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="requerimientos_' . $d->format ( 'd/m/Y' ) . '.pdf"'
            )
        );
    }

    /**
     * @Route("/pdf/requerimientos/secretaria", name="pdf_requerimientos_secretaria")
     */
    public function requerimientosSecretariaAction(Request $request)
    {
        try{

            $id = $request->get('id');
            $tipoImpresion = $request->get('tipoImpresion');
            $requerimientos = null;
            $bloque = null;
            $interbloque = null;
            $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

            $informeActual = $apiUtils->getUltimoInforme();


            if($tipoImpresion == "bloque"){
                $bloque = $apiUtils->getBloqueById($id);
                $envioReq = $apiUtils->getEnvioRequerimientoByBloque($id);
                $expediente = $informeActual["expediente"] == null? "" : "Exp. ".$informeActual["expediente"];
                $impresionNombre = $bloque["nombre"];
                $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($id);
            }else{
                $interbloque = $apiUtils->getInterbloqueById($id);
                $envioReq = $apiUtils->getEnvioRequerimientoByInterbloque($id);
                $expediente = $informeActual["expediente"] == null? "" : "Exp. ".$informeActual["expediente"];
                $impresionNombre = $interbloque["nombre"];
                $requerimientos = $apiUtils->getRequerimientosByInterbloqueInformeActivo($id);

            }

            $rdir = $this->get('kernel')->getRootDir() . "/../vendor/bin/wkhtmltopdf-amd64";

            $snappy = new Pdf($rdir,array("encoding" => "utf-8",
                'footer-right'=>utf8_decode(date('\ d/m/Y\ H:i')),
                'footer-left'=>utf8_decode($expediente),
                'header-html'=>utf8_decode($this->renderView('pdf_header.html.twig',array("impresion_nombre"=>$impresionNombre))),
                'header-spacing' => 35,
                'margin-top' => 50,
            ));



            $html = $this->renderView('requerimientos_impresion_secretaria.html.twig',array("requerimientos"=>$requerimientos,
                                                                                                    "tipo"=>$tipoImpresion,
                                                                                                    "bloque"=>$bloque,
                                                                                                    "interbloque"=>$interbloque));

        }catch(\Exception $e){
            die("Error: " . $e->getMessage());
        }
        $d = new \DateTime ();
        return new Response ( $this->makePdf ( $html, $snappy ), 200, array (
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="requerimientos_' . $d->format ( 'd/m/Y' ) . '.pdf"'
            )
        );
    }

    /**
     * @Route("/pdf/envio/requerimientos", name="pdf_envio_requerimientos")
     */
    public function envioRequerimientosAction(Request $request)
    {
        try{

            $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));
            $rdir = $this->get('kernel')->getRootDir() . "/../vendor/bin/wkhtmltopdf-amd64";
            //$snappy = new Pdf($rdir,array("encoding" => "utf-8"));
            $user = $this->get('session')->get('user');

            $informeActual = $apiUtils->getUltimoInforme();

            $snappy = new Pdf($rdir,array("encoding" => "utf-8",
                'footer-right'=>utf8_decode(date('\ d/m/Y\ H:i')),
                'header-html'=>utf8_decode($this->renderView('pdf_header.html.twig',array("user"=>$user,
                "informe"=>$informeActual))),
                'header-spacing' => 35,
                'margin-top' => 50,
            ));

            $user = $this->get('session')->get('user');
            $requerimientos = null;

            if($user["role"] == $_ENV["ROL_RESPONSABLE_BLOQUE"]){
                $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($user["extra"]["bloque"]["id"]);
            }else{
                $requerimientos = $apiUtils->getRequerimientosByInterbloqueInformeActivo($user["extra"]["interbloque"]["id"]);
            }

            $envioRequerimiento = $apiUtils->getEnvioRequerimientoByUser($this->get('session')->get('user'));

            $data = $envioRequerimiento["token"];
            $crypted = "asd123";
            $pk = openssl_pkey_get_private(
                "-----BEGIN RSA PRIVATE KEY-----
MIIEpgIBAAKCAQEAr8tj4fXX/1b8MHvl4UvGZP1YNRDFFADqXCZDvoifIgVZ4P3O
J8vZ76DEBohoCUnLqAHLAQoBhu2QempwGQRkbFGOTJqZhhAaZXvQZextuY0GveCb
ZFIf+oa4+ED5++u90NBdcEMHTC0xm4dEdxMRijPrgKfE36tA2kg4RvyvhF+X3oiH
xHYxtpqmJw2H6jVMIfItkfiYyhpCDcx7DDEDUHb8vwZv4RwEWYcTQket5QW5q0TE
rrES/nP1AKgSZif3UGQxiH5753pSAb62cYfi8Hj6OYA8U3TPZFh2Wj4Gbn1jDk2z
QsVY+Ne7fX6jP43OdaxGdFwW5h6dCvieNbYu8QIDAQABAoIBAQCr814336jvcb4s
DoTE4sGkdsqY3VZoQpz6ldMdw7UkKefIU3obIyKBaddULIjWzQAPzfEriwYDXMw9
cW9EiS9RZFS0SWRx5b5sFbe6epC5NmCpydE1R79P5gd3jNb4u5ZncehzrpysvmgP
oTgu4M//RtnMdk30GibNXGbvyKpF5zuLWcuUQFqWCo/Ux1R+jaRkZMLyznG6MHmx
JWDToU7QAcdPwVkZzeQijbok2VAjd8id4/b4Dfqbn6AiPCj0+tGxNDEv9kDojTff
reg1MIYio9OWqEQ/QlhTxk0kvIqbE16GGWTsnXsrUSZNzfBvaA0RpceleRkfnmW1
8hxN1VMhAoGBAOC1mGULakB0B93CrKLRjqDd7Z+vco8CKE4TKuMp1x0ItVON1Tfi
IoImL88VuUbr9jm0GpcVPpfCuQd/fu7j5ENtc497tynq/IB+A7jX+5EAzNxJT4uu
sf+gSN3mTCW+tA7JeuUf8C4HmQjIrh/KnEEJvUJZK8UIGQqy+cxcPfADAoGBAMhG
Fdaxtv9CfSmUr/YZ43RWvBk+fx6LdWdmpsLRgYmPzAv4Mbn669fR0/DgBtvJtrtV
lcKGe3K6jh8cy2wXIKEFY3HjN2QCgVS83MrKO+znW6WUc6xtp/oSmEQIsDocYtVV
TxpncpOqPTCmW2YbbE+gkp30h1HtpXNWNP6kE/T7AoGBAKpP+Nlc9YM0bmHeEwq0
2e2DR38ocyOA9xt126OhKrGO0CAr99cLRixu1P2X8X2Xil/h6eVo7LAJnkPYllRp
1bBlXJj7o6/ZsteQqzGGQd19RcgQqIGuPqsIyTGmQ0C4cbBjdQoEUv4QNCU8NP1T
i2qf/xoyi8IiiRha3onNwAQRAoGBALB3ILMF8CrG2SYUP19nvEU8T40mxjeRxicQ
CgxLMgqVy8MYNCidccW+uzLZEvICsKIsbULz4D5dJvE/G/boAfYfm1o0TRAwLOkP
aLS+sUKzMMubHLln/cSZMdVwmmb5sBQtkP/A+vIl5LjDPHGh5DPdVpMbbUjBTzFr
WlKtHHCVAoGBAMrxpB+DQSMH3tvP+4sCnEQAq+WP/kUznhsjQIMsKDXrg8tkAaLu
TU6i6PqAz79Fe3hLxftlEJYogMk47SmAAqcsae8+E1PYdsGC46K0rDPOaU/zHzM6
Lhw7TIPlQkRgQmBCiJdds8xR2X0llDs7Js8p56+ADe7QqowDR+8ih5od
-----END RSA PRIVATE KEY-----"
            );
            $result = openssl_private_encrypt($data, $crypted, $pk, OPENSSL_PKCS1_PADDING);
            if($result==false){
                die("Fail");
            }

            $envioRequerimiento["token"] = base64_encode($crypted);

            $cantPreguntas = 0;
            foreach ($requerimientos as $r) {
                $cantPreguntas = $cantPreguntas + sizeof($r["preguntas"]);
            }

            $html = $this->renderView('pdf_qr_envio_requerimientos.html.twig',array("envioRequerimiento"=>$envioRequerimiento,
                                                                                            "requerimientos"=>$requerimientos,
                                                                                            "cantPreguntas"=>$cantPreguntas));

        }catch(\Exception $e){
            die("Error impresión: " . $e->getMessage());
        }

        /*return $this->render(
            'pdf_qr_envio_requerimientos.html.twig',
            array(
                "envioRequerimiento"=>$envioRequerimiento
            )
        );*/

        $d = new \DateTime ();
        return new Response ( $this->makePdf ( $html, $snappy ), 200, array (
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="requerimientos_'. str_replace(" ","_",strtolower($user["extra"]["bloque"]["nombre"]) ).'_'. $d->format ( 'd/m/Y' ) . '.pdf"'
            )
        );
    }

    private function makePdf($html, $snappy) {
        srand ( $this->make_seed () );
        $token = md5 ( rand () );
        $current = $this->get ( 'kernel' )->getRootDir () . '/../temp/' . $token . '.pdf';
        //die($current);
        try {
            $snappy->generateFromHtml ( $html, $current );
        } catch ( \Exception $e ) {
            // NO LE DOY BOLA A LA EXCEPTION POR AHORA
            die("X: " . $e->getMessage());
        }
        $contenido = file_get_contents ( $current );
        return $contenido;
    }

    private function make_seed() {
        list ( $usec, $sec ) = explode ( ' ', microtime () );
        return ( float ) $sec + (( float ) $usec * 100000);
    }

    private function getRequerimientosByRole($role, $request){

        //die($role);

        $requerimientos = null;
        $apiUtils = new ApiUtil($this->get('session'), $request, $this->get('service_container'));

        switch ($role) {
            case $_ENV['ROL_DIPUTADO']:
                $requerimientos = $apiUtils->getRequerimientosByDiputadoInformeActivo($this->get('session')->get('user')["cuil"]);
                break;
            case $_ENV['ROL_ASESOR']:
                $requerimientos = $apiUtils->getRequerimientosByDiputadoInformeActivo($this->get('session')->get('user')["extra"]["diputado"]["cuil"]);
                break;
            case $_ENV['ROL_PERSONAL_PARLAMENTARIO']:
                $requerimientos = $apiUtils->getRequerimientosByUser($this->get('session')->get('user'));
                break;
            case $_ENV['ROL_RESPONSABLE_BLOQUE']:
                $requerimientos = $apiUtils->getRequerimientosByBloqueInformeActivo($this->get('session')->get('user')["extra"]["bloque"]["id"]);
                //print_r($requerimientos);
                //die("");
                break;
            case $_ENV['ROL_RESPONSABLE_INTERBLOQUE']:
                $requerimientos = $apiUtils->getRequerimientosByInterbloqueInformeActivo($this->get('session')->get('user')["extra"]["interbloque"]["id"]);
                break;
            case $_ENV['ROL_ADMIN']:
                $requerimientos = null;
                break;
            case $_ENV['ROL_DIRECCION_SECRETARIA']:
                //no hago nada
                break;
        }

        return $requerimientos;

    }

    private function getHttpDir(Request $request, $filename){
        return $request->getSchemeAndHttpHost()."/".$request->getBasePath()."/".$filename;
    }

}
