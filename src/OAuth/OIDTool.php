<?php

namespace App\OAuth;

use Firebase\JWT\JWT;

class OIDTool{

    private $authServer = "https://auth.hcdn.gob.ar/authorize";
    private $tokenServer = "https://auth.hcdn.gob.ar/token";
    private $clientId = null;
    private $redirectUri = null;
    private $clientSecret = null;
    private $scope = "openid";
    private $state = "";
    
    public function __construct($clientId, $redirectUri, $clientSecret) {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();   
        }
        $this->clientId = $clientId;
        $this->redirectUri = $redirectUri;
        $this->clientSecret = $clientSecret;
    }
    
    public function setState($state){
        $this->state = $state;
    }

    public function getAuthorizationUrl(){
        /* Renew State */
        $this->state = uniqid();
        $_SESSION["oid.state"] = $this->state;
        $auth = array(
            "client_id" => $this->clientId,
            "response_type" => "code",
            "redirect_uri" => $this->redirectUri,
            "state" => $this->state,
            "scope" => "openid"
        );
        $parameters = http_build_query($auth);
        return $this->authServer . '?' . $parameters;
    }
    
    public function stateIsValid($state){
        return true;
        if(!isset($_SESSION["oid.state"])||empty($_SESSION["oid.state"])){
            return false;
        }
        $curr = $_SESSION["oid.state"];
        if($state!=$curr){
            return false;
        }
        return true;
    }
    
    public function getUserCredentials($code, $publicKeyDir){
        $publicKeyContent = file_get_contents($publicKeyDir);
        $client = new \GuzzleHttp\Client();
        $grantType = "authorization_code";
        $params = array(
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            "redirect_uri" => $this->redirectUri,
            "grant_type" => $grantType,
            "code" => $code
        );
        
        $result = $client->request('POST', $this->tokenServer, [ 'form_params' => $params ]);
        $cts = $result->getBody()->getContents();
        $j = json_decode($cts, true);
        /* j tiene datos como el access token, la expiración de mismo y el JWT */
        $idToken = $j["id_token"];//Obtengo el JWT que tiene datos de usuario y expiración de ID TOKEN.
        $jwt = JWT::decode($idToken, $publicKeyContent, array('RS256'));
        $jwt = (array) $jwt;
        
        return $jwt;
    }
    
}