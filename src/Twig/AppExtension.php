<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('price', array($this, 'priceFilter')),
        );
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('GET_ENV', [$this, 'getEnv']),
        ];
    }

    public function getEnv($varname)
    {
        $value = getenv($varname);

        // Do something with $value...

        return $value;
    }

}