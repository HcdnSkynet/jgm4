<?php

namespace App\Repository;

use App\Entity\JgmLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JgmLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method JgmLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method JgmLog[]    findAll()
 * @method JgmLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JgmLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JgmLog::class);
    }

    // /**
    //  * @return JgmLog[] Returns an array of JgmLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JgmLog
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
